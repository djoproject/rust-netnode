use crate::utils::buffer;

use crossbeam_channel;

use log::error;

pub struct Pool {
    receiver: crossbeam_channel::Receiver<Vec<u8>>,
    sender: crossbeam_channel::Sender<Vec<u8>>,
    size: usize,
}

impl Pool {
    pub fn new(size: usize) -> Self {
        let receiver: crossbeam_channel::Receiver<Vec<u8>>;
        let sender: crossbeam_channel::Sender<Vec<u8>>;

        (sender, receiver) = crossbeam_channel::bounded::<Vec<u8>>(size);
        return Self {
            receiver,
            sender,
            size,
        };
    }

    pub fn get_next_available_buffer(&self) -> Option<buffer::Buffer> {
        if let Ok(buffer) = self.receiver.recv() {
            return Some(buffer::Buffer::new(buffer, self.sender.clone()));
        }

        // XXX the pool is closed, returning None will tell the consumer to shut down
        None
    }

    fn add_buffer(&self, buffer: Vec<u8>) {
        if let Err(_) = self.sender.try_send(buffer) {
            error!("Attempt to add a buffer while the pool is full.  Buffer discarded.");
        }
    }

    pub fn len(&self) -> usize {
        return self.size;
    }

    // XXX No close method for now.  Why ? the implementation of the current chanel does not allow to
    // close the chanel for all threads at once.  Need to find another approach to shut down
    // every pool consumer.
}

impl Clone for Pool {
    #[inline]
    fn clone(&self) -> Self {
        return Self {
            receiver: self.receiver.clone(),
            sender: self.sender.clone(),
            size: self.size,
        };
    }
}

pub fn create_buffer_pool(pool_size: usize, buffer_size: usize) -> Pool {
    let pool: Pool = Pool::new(pool_size);

    for _ in 0..pool_size {
        pool.add_buffer(Vec::with_capacity(buffer_size));
    }

    pool
}
