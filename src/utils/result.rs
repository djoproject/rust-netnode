use crate::layers;
use crate::layers::l3::icmpv4;
use crate::layers::l3::icmpv6;

use std::fmt;
use std::io;

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub struct Error {
    kind: ErrorKind,
}

impl Error {
    pub fn new(kind: ErrorKind) -> Self {
        return Self { kind };
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.kind)
    }
}

impl std::error::Error for Error {}

#[derive(Debug)]
pub enum ErrorKind {
    ConfigError,
    SettingError,
    UnsupportedIcmpV4Type(icmpv4::IcmpType),
    UnsupportedIcmpV6Type(icmpv6::IcmpType),
    MethodNotSupportedForThisLayer(&'static str, &'static str),
    WrongLengthAddress(usize, usize),
    UnsuportedProcessMetaType(layers::ProcessMetaType),
    DeviceOpened,
    DeviceClosed,
    IoError(io::Error),
    InvalidStringAddress(String),
    UnknownOsError(&'static str),
    OsError(&'static str, i32),
}

impl fmt::Display for ErrorKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ErrorKind::ConfigError => write!(f, "A config error occured"),
            ErrorKind::SettingError => write!(f, "A setting error occured"),
            ErrorKind::UnsupportedIcmpV4Type(type_) => write!(
                f,
                "The ICMPv4 type {} is currently not supported, don't try to enable it",
                type_
            ),
            ErrorKind::UnsupportedIcmpV6Type(type_) => write!(
                f,
                "The ICMPv6 type {} is currently not supported, don't try to enable it",
                type_
            ),
            ErrorKind::MethodNotSupportedForThisLayer(layer, method) => {
                write!(f, "The layer {} does not support method {}", layer, method)
            }
            ErrorKind::WrongLengthAddress(expected, got) => {
                write!(f, "Expected a {} bytes address, got {}", expected, got)
            }
            ErrorKind::UnsuportedProcessMetaType(meta_type) => {
                write!(f, "Unsuported meta process type, got {}", meta_type)
            }
            ErrorKind::DeviceOpened => {
                write!(f, "Unsuported operation, device is opened")
            }
            ErrorKind::DeviceClosed => {
                write!(f, "Unsuported operation, device is closed")
            }
            ErrorKind::IoError(err) => {
                write!(f, "IO error: {}", err)
            }
            ErrorKind::InvalidStringAddress(reason) => {
                write!(f, "The string provided is not a valid address: {}", reason)
            }
            ErrorKind::UnknownOsError(msg) => {
                write!(f, "{}: An unknown OS error has occured", msg)
            }
            ErrorKind::OsError(msg, err_num) => unsafe {
                let c_char = libc::strerror(*err_num);
                let cstr = std::ffi::CStr::from_ptr(c_char);
                let s = String::from_utf8_lossy(cstr.to_bytes()).to_string();
                write!(f, "{}: OS error: {}", msg, s)
            },
        }
    }
}
