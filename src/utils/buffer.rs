use std::ops::Deref;
use std::ops::DerefMut;
use std::ops::{Index, IndexMut, Range, RangeFrom};

use crossbeam_channel;

pub struct Buffer {
    buffer: Option<Vec<u8>>,
    sender: crossbeam_channel::Sender<Vec<u8>>,
}

impl Buffer {
    pub fn new(buffer: Vec<u8>, sender: crossbeam_channel::Sender<Vec<u8>>) -> Self {
        return Self {
            buffer: Some(buffer),
            sender,
        };
    }

    pub fn set_len(&mut self, size: usize) {
        match &mut self.buffer {
            Some(b) => unsafe { b.set_len(size) },
            None => panic!("Trying to use a released buffer"),
        }
    }

    pub fn capacity(&self) -> usize {
        match &self.buffer {
            Some(b) => b.capacity(),
            None => panic!("Trying to use a released buffer"),
        }
    }

    pub fn len(&self) -> usize {
        match &self.buffer {
            Some(b) => b.len(),
            None => panic!("Trying to use a released buffer"),
        }
    }

    #[inline(always)]
    pub fn to_header<H>(&self, start: usize) -> &H {
        match &self.buffer {
            Some(b) => unsafe { &*(b[start..].as_ptr() as *const H) },
            None => panic!("Trying to use a released buffer"),
        }
    }

    #[inline(always)]
    pub fn to_header_mut<H>(&mut self, start: usize) -> &mut H {
        match &self.buffer {
            Some(b) => unsafe { &mut *(b[start..].as_ptr() as *mut H) },
            None => panic!("Trying to use a released buffer"),
        }
    }
}

impl Drop for Buffer {
    fn drop(&mut self) {
        if let Some(val) = self.buffer.take() {
            if let Err(crossbeam_channel::SendError(val)) = self.sender.send(val) {
                // if no way to release the buffer to the pool, deallocate it.
                drop(val);
            }
        }
    }
}

impl AsRef<[u8]> for Buffer {
    #[inline(always)]
    fn as_ref(&self) -> &[u8] {
        match &self.buffer {
            None => &[],
            Some(b) => b.as_ref(),
        }
    }
}

impl AsMut<[u8]> for Buffer {
    #[inline(always)]
    fn as_mut(&mut self) -> &mut [u8] {
        match &mut self.buffer {
            None => &mut [],
            Some(b) => b.as_mut(),
        }
    }
}

impl Index<usize> for Buffer {
    type Output = u8;
    fn index(&self, i: usize) -> &u8 {
        match &self.buffer {
            Some(b) => &b[i],
            None => panic!(""),
        }
    }
}

impl IndexMut<usize> for Buffer {
    #[inline(always)]
    fn index_mut(&mut self, i: usize) -> &mut u8 {
        match &mut self.buffer {
            Some(b) => &mut b[i],
            None => panic!(""),
        }
    }
}

impl Index<Range<usize>> for Buffer {
    type Output = [u8];
    #[inline(always)]
    fn index(&self, r: Range<usize>) -> &[u8] {
        match &self.buffer {
            Some(b) => &b[r.start..r.end],
            None => panic!(""),
        }
    }
}

impl IndexMut<Range<usize>> for Buffer {
    fn index_mut(&mut self, r: Range<usize>) -> &mut [u8] {
        match &mut self.buffer {
            Some(b) => &mut b[r.start..r.end],
            None => panic!(""),
        }
    }
}

impl Index<RangeFrom<usize>> for Buffer {
    type Output = [u8];
    fn index(&self, r: RangeFrom<usize>) -> &[u8] {
        match &self.buffer {
            Some(b) => &b[r.start..],
            None => panic!(""),
        }
    }
}

impl IndexMut<RangeFrom<usize>> for Buffer {
    fn index_mut(&mut self, r: RangeFrom<usize>) -> &mut [u8] {
        match &mut self.buffer {
            Some(b) => &mut b[r.start..],
            None => panic!(""),
        }
    }
}

impl Deref for Buffer {
    type Target = [u8];
    fn deref(&self) -> &[u8] {
        match &self.buffer {
            Some(b) => &b,
            None => panic!(""),
        }
    }
}

impl DerefMut for Buffer {
    fn deref_mut(&mut self) -> &mut [u8] {
        match &mut self.buffer {
            Some(b) => b.as_mut(),
            None => panic!(""),
        }
    }
}
