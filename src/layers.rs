use crate::utils::buffer;
use crate::utils::pool;

use std::fmt;

pub mod l1;
pub mod l2;
pub mod l3;

pub struct ProcessSystem {
    pub pool: pool::Pool,
}

impl ProcessSystem {
    pub fn new(size: usize) -> Self {
        Self {
            // TODO(buffer) buffer size should not be hardcoded here
            pool: pool::create_buffer_pool(size, 1540),
        }
    }

    pub fn get_next_available_buffer(&mut self, size: usize) -> Option<buffer::Buffer> {
        let buffer_option = self.pool.get_next_available_buffer();

        if let None = buffer_option {
            return None;
        }

        let mut answer = buffer_option.unwrap();
        answer.set_len(size);

        Some(answer)
    }
}

impl Clone for ProcessSystem {
    #[inline]
    fn clone(&self) -> Self {
        Self {
            pool: self.pool.clone(),
        }
    }
}

#[derive(PartialEq, Eq, Clone, Debug, Copy)]
pub enum ProcessMetaType {
    TunTap,
    Ethernet,
    ARP,
    IPv4,
    IPv6,
    ICMPv4,
    ICMPv6,
    UnManaged,
    Unknown,
}

impl fmt::Display for ProcessMetaType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ProcessMetaType::TunTap => write!(f, "TunTap"),
            ProcessMetaType::Ethernet => write!(f, "Ethernet"),
            ProcessMetaType::ARP => write!(f, "ARP"),
            ProcessMetaType::IPv4 => write!(f, "IPv4"),
            ProcessMetaType::IPv6 => write!(f, "IPv6"),
            ProcessMetaType::ICMPv4 => write!(f, "ICMPv4"),
            ProcessMetaType::ICMPv6 => write!(f, "ICMPv6"),
            ProcessMetaType::UnManaged => write!(f, "UnManaged"),
            ProcessMetaType::Unknown => write!(f, "Unknown"),
        }
    }
}

pub struct ProcessMeta<'a> {
    start_at: usize,
    answer_at: usize,
    previous: Option<&'a ProcessMeta<'a>>,
    previous_type: Option<ProcessMetaType>,
}

pub struct ProcessResult {
    // TODO(result) add field, use them
    //  * bool: needToUpdate
    //
}

impl<'a> ProcessMeta<'a> {
    pub fn new(start_at: usize, answer_at: usize) -> Self {
        Self {
            start_at,
            answer_at,
            previous: None,
            previous_type: None,
        }
    }

    pub fn new_origin() -> Self {
        Self {
            start_at: 0,
            answer_at: 0,
            previous: None,
            previous_type: None,
        }
    }

    pub fn from_meta(previous: &'a Self, offset: usize, previous_type: ProcessMetaType) -> Self {
        // XXX this method allow to set the offset for header in request and in response.
        // inferring the needed response space is the same as the request space.
        // An other method could be needed for the case where thoses spaces are different.
        return Self {
            start_at: previous.start_at + offset,
            answer_at: previous.answer_at + offset,
            previous: Some(previous),
            previous_type: Some(previous_type),
        };
    }

    pub fn get_previous(&self) -> Option<&'a ProcessMeta<'a>> {
        self.previous
    }

    pub fn get_previous_type(&self) -> Option<ProcessMetaType> {
        self.previous_type
    }
}

pub trait Layer: Send + Sync {
    fn get_meta_type(&self) -> ProcessMetaType;

    fn process<'a>(
        &self,
        system: &'a mut ProcessSystem,
        buffer: buffer::Buffer,
        meta: ProcessMeta,
    ) -> (Option<ProcessResult>, Option<buffer::Buffer>);
    // TODO(result) the return value should probably evolve to a Result<Error, Option<&'a mut [u8]>>
    //  and also put a ResultMeta with petinent information for the lower layer
}

pub trait Protocol: fmt::Display {
    fn get_meta_type(&self) -> ProcessMetaType;
}

pub trait Header: fmt::Display {}
