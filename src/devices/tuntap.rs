use crate::devices;
use crate::devices::cloned_tuntap::ClonedInterface;
use crate::layers::l1::ip_command::set_interface_status;

use std::fs;
use std::io;
use std::io::ErrorKind;
use std::os::unix::io::{AsRawFd, FromRawFd, RawFd};

use tun_tap::Iface;
use tun_tap::Mode;

pub struct Interface {
    template_name: &'static str,
    interface_mode: Mode,
    pub interface: Option<Iface>,
}

impl Interface {
    pub fn new_tap() -> Self {
        return Self {
            template_name: "tap%d",
            interface_mode: Mode::Tap,
            interface: None,
        };
    }

    pub fn new_tun() -> Self {
        return Self {
            template_name: "tun%d",
            interface_mode: Mode::Tun,
            interface: None,
        };
    }
}

impl devices::Device for Interface {
    fn open(&mut self) -> io::Result<()> {
        if self.interface.is_some() {
            return Err(io::Error::new(
                ErrorKind::AlreadyExists,
                "Interface already opened",
            ));
        }

        let iface = Iface::new(self.template_name, self.interface_mode)?;
        set_interface_status(iface.name(), true)?;
        self.interface = Some(iface);

        Ok(())
    }

    fn is_open(&self) -> bool {
        self.interface.is_some()
    }

    fn name(&self) -> &str {
        match &self.interface {
            Some(interface) => interface.name(),
            None => "Interface not opened",
        }
    }

    fn read(&self, buffer: &mut [u8]) -> io::Result<usize> {
        if self.interface.is_none() {
            return Err(io::Error::new(
                ErrorKind::NotConnected,
                "Interface not opened",
            ));
        }

        self.interface.as_ref().unwrap().recv(buffer)
    }

    fn write(&self, buffer: &[u8]) -> io::Result<usize> {
        if self.interface.is_none() {
            return Err(io::Error::new(
                ErrorKind::NotConnected,
                "Interface not opened",
            ));
        }

        self.interface.as_ref().unwrap().send(buffer)
    }

    fn close(&mut self) -> io::Result<()> {
        if self.interface.is_none() {
            return Err(io::Error::new(
                ErrorKind::NotConnected,
                "Interface not opened",
            ));
        }

        set_interface_status(self.interface.as_ref().unwrap().name(), false)?;

        self.interface = None;
        Ok(())
    }

    fn clone(&self) -> Box<dyn devices::Device> {
        // XXX this clone method has not the same semantic if the interface is open or not.
        //  That could be an issue later.
        //
        // If the interface is close, it copy it but not link it.
        // If the interface is open, it does not copy it but link it.

        match self.interface.as_ref() {
            Some(v) => {
                // XXX ugly way of extracting file descriptor from the tun/tap interface, but the only
                // way I've found so far...

                let file = unsafe { fs::File::from_raw_fd(v.as_raw_fd()) };
                return Box::new(ClonedInterface::new(self.name().to_string(), file));
            }
            None => {
                return Box::new(Self {
                    template_name: self.template_name,
                    interface_mode: self.interface_mode,
                    interface: None,
                });
            }
        }
    }
}

impl AsRawFd for Interface {
    fn as_raw_fd(&self) -> RawFd {
        match self.interface.as_ref() {
            Some(v) => v.as_raw_fd(),
            None => panic!("can't get raw fd for a closed interface."),
        }
    }
}
