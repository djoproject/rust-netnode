use crate::devices;

use std::fs;
use std::io;
use std::io::{Read, Write};
use std::mem;
use std::os::unix::io::{AsRawFd, RawFd};

pub struct ClonedInterface {
    interface_name: String,
    interface: Option<fs::File>,
}

impl ClonedInterface {
    pub fn new(name: String, file: fs::File) -> Self {
        Self {
            interface_name: name,
            interface: Some(file),
        }
    }
}

impl devices::Device for ClonedInterface {
    fn open(&mut self) -> io::Result<()> {
        Ok(())
    }

    fn is_open(&self) -> bool {
        match self.interface.as_ref() {
            None => return false,
            Some(fd) => unsafe {
                let ret = libc::fcntl(fd.as_raw_fd().into(), libc::F_GETFL);
                return ret >= 0;
            },
        }
    }

    fn name(&self) -> &str {
        return &self.interface_name;
    }

    fn read(&self, buffer: &mut [u8]) -> io::Result<usize> {
        let mut file = self.interface.as_ref().unwrap();
        file.read(buffer)
    }

    fn write(&self, buffer: &[u8]) -> io::Result<usize> {
        let mut file = self.interface.as_ref().unwrap();
        file.write(buffer)
    }

    fn close(&mut self) -> io::Result<()> {
        Ok(())
    }

    fn clone(&self) -> Box<dyn devices::Device> {
        let file = match self.interface.as_ref().unwrap().try_clone() {
            Err(_) => panic!("failed to clone interface"),
            Ok(value) => value,
        };

        return Box::new(Self {
            interface_name: self.interface_name.clone(),
            interface: Some(file),
        });
    }
}

impl Drop for ClonedInterface {
    fn drop(&mut self) {
        if let Some(val) = self.interface.take() {
            // Use this in thread to avoid closing the file if the thread is stopped.
            // The real owner of the file is self.interface.
            mem::forget(val);
        }
    }
}

impl AsRawFd for ClonedInterface {
    fn as_raw_fd(&self) -> RawFd {
        self.interface.as_ref().unwrap().as_raw_fd()
    }
}
