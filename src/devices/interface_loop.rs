use crate::devices;
use crate::devices::poller;
use crate::layers;
use crate::utils::result;

use log::{error, info, warn};

use std::thread;

pub struct HighLevelInterface<'a> {
    device: Box<dyn devices::Device>,
    system: &'a layers::ProcessSystem,
    output_sender: Option<crossbeam_channel::Sender<devices::MessageType>>,
    input_thread_handler: Option<thread::JoinHandle<()>>,
    output_thread_handler: Option<thread::JoinHandle<()>>,
}

impl<'a> HighLevelInterface<'a> {
    pub fn new(device: Box<dyn devices::Device>, system: &'a layers::ProcessSystem) -> Self {
        Self {
            device,
            system,
            output_sender: None,
            input_thread_handler: None,
            output_thread_handler: None,
        }
    }

    fn start_output_thread(
        &self,
        output_receiver: crossbeam_channel::Receiver<devices::MessageType>,
    ) -> thread::JoinHandle<()> {
        let device = self.device.clone();

        thread::spawn(move || loop {
            match output_receiver.recv() {
                Ok(message_type) => match message_type {
                    devices::MessageType::Data(buffer) => {
                        if let Err(err) = device.write(buffer.as_ref()) {
                            error!(
                                "Failed to write buffer on interface {}: {}",
                                device.name(),
                                err
                            );
                            break;
                        }
                    }
                    devices::MessageType::Control(command) => match command {
                        devices::MessageControl::Close => {
                            info!("Receive order to close output thread");
                            break;
                        }
                    },
                },
                Err(_) => {
                    warn!("Output buffer closed for interface {}", device.name());
                    break;
                }
            }
        })
    }

    fn start_input_thread(&self, process_obj: Box<dyn layers::Layer>) -> thread::JoinHandle<()> {
        let device = self.device.clone();
        let mut system = self.system.clone();
        let output_sender = self.output_sender.as_ref().unwrap().clone();

        thread::spawn(move || {
            let mut poller = poller::Poller::new();
            if let Err(e) = poller.open() {
                error!("Failed to init poller: {}", e);
                return;
            }

            if let Err(e) = poller.add_file(device.as_raw_fd()) {
                error!("Failed to add interface to poller: {}", e);
                return;
            }

            loop {
                // Polling the interface
                match poller.poll() {
                    Ok(is_there_content) => {
                        if !is_there_content {
                            // no content, so timeout.
                            // Only check if the device is still open when the polling timeout
                            // occurs.  If the interface is closed, no more data will arrive and
                            // the timeout will eventually occur.
                            if !device.is_open() {
                                info!(
                                    "Closing interface {}, reason: interface is closed",
                                    device.name()
                                );
                                break;
                            }
                            continue;
                        }
                    }
                    Err(e) => {
                        error!("Closing interface {}, reason: {}", device.name(), e);
                        break;
                    }
                }

                // Buffer management
                let buffer = system.pool.get_next_available_buffer();
                if buffer.is_none() {
                    info!(
                        "Closing interface {}, reason: buffer pool is closed",
                        device.name()
                    );
                    break;
                }

                let mut buffer = buffer.unwrap();
                buffer.set_len(buffer.capacity());

                // Reading
                let size;
                match device.read(buffer.as_mut()) {
                    Err(err) => {
                        error!(
                            "Failed to read frame on interface {}: {}",
                            device.name(),
                            err
                        );
                        break;
                    }
                    Ok(embedded_size) => size = embedded_size,
                }
                buffer.set_len(size);

                // Processing
                let meta = layers::ProcessMeta::new_origin();
                let (_, answer_option) = process_obj.process(&mut system, buffer, meta);

                if let Some(answer) = answer_option {
                    if let Err(_) = output_sender.send(devices::MessageType::Data(answer)) {
                        warn!(
                            "Closing interface {}, reason: output buffer is closed",
                            device.name()
                        );
                        break;
                    }
                }
            }
        })
    }
}

impl devices::DeviceLoop for HighLevelInterface<'_> {
    fn start(&mut self, process_obj: Box<dyn layers::Layer>) -> result::Result<()> {
        if self.is_running() {
            return Err(result::Error::new(result::ErrorKind::DeviceOpened));
        }

        if !self.device.is_open() {
            if let Err(err) = self.device.open() {
                return Err(result::Error::new(result::ErrorKind::IoError(err)));
            }
        }

        // XXX the input buffer of an interface is the pool full size, meaning an interface
        // can hold every buffer in its output buffer.  The size of this is marginal because
        // only the pointers are going to be stored in that queue.
        let (output_sender, output_receiver) = crossbeam_channel::bounded(self.system.pool.len());
        self.output_sender = Some(output_sender);

        self.output_thread_handler = Some(self.start_output_thread(output_receiver));

        self.input_thread_handler = Some(self.start_input_thread(process_obj));

        Ok(())
    }

    fn stop(&mut self) -> result::Result<()> {
        if !self.is_running() {
            return Err(result::Error::new(result::ErrorKind::DeviceClosed));
        }

        // manage self.device closing
        if let Err(err) = self.device.close() {
            return Err(result::Error::new(result::ErrorKind::IoError(err)));
        }

        // send a message to the output buffer to close
        let msg = devices::MessageType::Control(devices::MessageControl::Close);

        // don't care if there is an error, that means the channel is already closed and so the
        // thread is probably already dead.
        let _ = self.output_sender.as_ref().unwrap().send(msg);

        // Drop channel
        self.output_sender = None;
        Ok(())
    }

    fn is_running(&self) -> bool {
        match &self.output_thread_handler {
            None => false,
            Some(handler) => !handler.is_finished(),
        }
    }

    fn join(&mut self) {
        // XXX Because HighLevelInterface is not clonable, no different thread could call this
        // method, so no risk of race condition

        // XXX it would be nice to get the panic message but Err(err) type is `dyn Any + Send`, I
        // don't really know how to extract info from that for now...

        if let Some(handler) = self.input_thread_handler.take() {
            if let Err(_) = handler.join() {
                error!("input thread panicked for interface {}", self.device.name());
            }
        }

        if let Some(handler) = self.output_thread_handler.take() {
            if let Err(_) = handler.join() {
                error!(
                    "output thread panicked for interface {}",
                    self.device.name()
                );
            }
        }
    }

    fn get_output_queue(&self) -> result::Result<&crossbeam_channel::Sender<devices::MessageType>> {
        if !self.is_running() {
            return Err(result::Error::new(result::ErrorKind::DeviceClosed));
        }

        Ok(self.output_sender.as_ref().unwrap())
    }
}
