use crate::utils::result;

use std::os::fd;

fn poll_error(s: &'static str) -> result::Error {
    match std::io::Error::last_os_error().raw_os_error() {
        None => result::Error::new(result::ErrorKind::UnknownOsError(s)),
        Some(errno) => result::Error::new(result::ErrorKind::OsError(s, errno)),
    }
}

pub struct Poller {
    efd: libc::c_int,
}

impl Poller {
    pub fn new() -> Self {
        return Self { efd: -1 };
    }

    pub fn open(&mut self) -> result::Result<()> {
        unsafe {
            // The obsolete size argument set to 1 has no meaning, it just needs to be different from 0.
            let epoll_fd = libc::epoll_create(1);

            if epoll_fd < 0 {
                return Err(poll_error("failed to init epool"));
            }

            self.efd = epoll_fd;
        }

        Ok(())
    }

    pub fn add_file(&self, fd: fd::RawFd) -> result::Result<()> {
        unsafe {
            let mut events = libc::epoll_event {
                events: libc::EPOLLIN as u32,
                u64: 0,
            };

            let ret = libc::epoll_ctl(self.efd, libc::EPOLL_CTL_ADD, fd, &mut events);
            if ret < 0 {
                return Err(poll_error("failed to add fd to epoll"));
            }
        }

        Ok(())
    }

    pub fn poll(&self) -> result::Result<bool> {
        let mut events = libc::epoll_event { events: 0, u64: 0 };

        unsafe {
            let ret = libc::epoll_wait(self.efd, &mut events, 10, 200);
            if ret < 0 {
                return Err(poll_error("failed wait with epoll"));
            }

            if (events.events & libc::EPOLLIN as u32) > 0 {
                return Ok(true);
            }

            Ok(false)
        }
    }
}

impl Drop for Poller {
    fn drop(&mut self) {
        unsafe {
            if self.efd > -1 {
                libc::close(self.efd);
            }
        }
    }
}
