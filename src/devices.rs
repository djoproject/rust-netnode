use crate::layers;
use crate::utils::buffer;
use crate::utils::result;

use std::io;
use std::os::unix::io::AsRawFd;

mod cloned_tuntap;
pub mod interface_loop;
mod poller;
pub mod tuntap;

pub enum MessageControl {
    Close,
}

pub enum MessageType {
    Data(buffer::Buffer),
    Control(MessageControl),
}

pub trait Device: AsRawFd + Send + Sync {
    fn open(&mut self) -> io::Result<()>;
    fn close(&mut self) -> io::Result<()>;
    fn is_open(&self) -> bool;
    fn name(&self) -> &str;
    fn read(&self, buffer: &mut [u8]) -> io::Result<usize>;
    fn write(&self, buffer: &[u8]) -> io::Result<usize>;
    fn clone(&self) -> Box<dyn Device>;
}

pub trait DeviceLoop {
    fn start(&mut self, process_obj: Box<dyn layers::Layer>) -> result::Result<()>;
    fn is_running(&self) -> bool;
    fn stop(&mut self) -> result::Result<()>;
    fn join(&mut self);
    fn get_output_queue(&self) -> result::Result<&crossbeam_channel::Sender<MessageType>>;
}
