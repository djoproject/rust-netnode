use rust_netnode::devices::interface_loop;
use rust_netnode::devices::tuntap;
use rust_netnode::devices::{Device, DeviceLoop};
use rust_netnode::layers;
use rust_netnode::layers::l1::ip_command;
use rust_netnode::layers::l1::tuntap as tuntap_process;
use rust_netnode::layers::l2::arp;
use rust_netnode::layers::l2::ethernet;
use rust_netnode::layers::l2::Layer as L2Layer;
use rust_netnode::layers::l3::icmpv4;
use rust_netnode::layers::l3::icmpv6;
use rust_netnode::layers::l3::ipv4;
use rust_netnode::layers::l3::ipv6;
use rust_netnode::layers::l3::Layer as L3Layer;
use rust_netnode::utils::result;

use std::error::Error;
use std::net::{IpAddr, Ipv4Addr, Ipv6Addr};
use std::str::FromStr;
use std::thread;
use std::time;

use log::LevelFilter;
use log::{debug, error, info, warn};

use clap::Parser;

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    /// Enable debug mode
    #[arg(short, long, default_value_t = false)]
    debug: bool,

    /// Mac address to use from the app side of the tunnel
    #[arg(short, long, default_value_t = String::from("06:00:00:00:00:05"))]
    mac: String,

    /// App side IPv4 address
    #[arg(short = '4', long, default_value_t = String::from("192.168.222.2"))]
    client_ipv4: String,

    /// App side IPv6 address (Link Local Address)
    #[arg(short = '6', long, default_value_t = String::from("fe80::22"))]
    client_ipv6: String,

    /// Kernel side IPv4 address
    #[arg(short, long, default_value_t = String::from("192.168.222.1"))]
    kernel_ipv4: String,

    /// Self kill after N seconds
    #[arg(short, long)]
    self_kill: Option<usize>,
}

fn build_l3_ipv4_layer(
    ipv4: Ipv4Addr,
    verify_checksum: bool,
    print_dropped: bool,
) -> Result<impl L3Layer, result::Error> {
    // ICMPv4 Management
    let mut icmpv4_process = icmpv4::Process::new(verify_checksum);
    icmpv4_process.enable_type(icmpv4::IcmpType::EchoRequest)?;

    // IPv4 management
    let mut ipv4_process = ipv4::Process::new(ipv4, verify_checksum);
    ipv4_process.add_protocol_process(ipv4::Protocol::ICMP, Box::new(icmpv4_process));
    if print_dropped {
        ipv4_process.set_dropped_process(Box::new(ipv4::DefaultSubProcess::new("Dropped")));
    }

    Ok(ipv4_process)
}

fn build_l3_ipv6_layer(
    mac: ethernet::Address,
    ipv6: Ipv6Addr,
    verify_checksum: bool,
    print_dropped: bool,
) -> Result<impl L3Layer, result::Error> {
    // ICMPv6 Management
    let mut icmpv6_process = icmpv6::Process::new(verify_checksum, mac);
    icmpv6_process.enable_type(icmpv6::IcmpType::EchoRequest)?;
    icmpv6_process.enable_type(icmpv6::IcmpType::NeighborSolicitation)?;

    // IPv6 management
    let mut ipv6_process = ipv6::Process::new(ipv6);
    ipv6_process.add_protocol_process(ipv4::Protocol::ICMPv6, Box::new(icmpv6_process));
    if print_dropped {
        ipv6_process.set_dropped_process(Box::new(ipv6::DefaultSubProcess::new("Dropped")));
    }

    Ok(ipv6_process)
}

fn build_l2_process(
    mac: ethernet::Address,
    ipv4: Ipv4Addr,
    ipv6: Ipv6Addr,
    verify_checksum: bool,
    print_dropped: bool,
) -> Result<impl layers::Layer, result::Error> {
    // Ethernet Management
    let mut ethernet_process = ethernet::Process::new(mac);

    // ARP management
    let mut arp_process = arp::Process::new(mac);
    arp_process.update_layer(&mut ethernet_process)?;

    // IPv4 management
    let mut ipv4_process = build_l3_ipv4_layer(ipv4, verify_checksum, print_dropped)?;
    ipv4_process.update_lower_layer(&mut arp_process)?;
    ipv4_process.update_lower_layer(&mut ethernet_process)?;

    // IPv6 management
    let mut ipv6_process = build_l3_ipv6_layer(mac, ipv6, verify_checksum, print_dropped)?;
    ipv6_process.update_lower_layer(&mut ethernet_process)?;

    // Add process to ethernet
    ethernet_process.add_etype_process(ethernet::EtherType::ARP, Box::new(arp_process));
    ethernet_process.add_etype_process(ethernet::EtherType::IPv4, Box::new(ipv4_process));
    ethernet_process.add_etype_process(ethernet::EtherType::IPv6, Box::new(ipv6_process));

    if print_dropped {
        // Catch-all LLC frame (for debug purpose)
        let llc_process = Box::new(ethernet::DefaultSubProcess::new("LLC"));
        ethernet_process.set_llc_default_process(llc_process);

        // Catch-all dropped frame (for debug purpose)
        let default_ethernet = Box::new(ethernet::DefaultSubProcess::new("Dropped"));
        ethernet_process.set_dropped_process(default_ethernet);
    }

    Ok(ethernet_process)
}

fn init_interface<'a>(
    client_mac: &'a str,
    client_ipv4: &'a str,
    client_ipv6: &'a str,
    kernel_ipv4: &'a str,
    system: &'a layers::ProcessSystem,
) -> Result<interface_loop::HighLevelInterface<'a>, Box<dyn Error>> {
    // Define local adresses (MAC, ipv4, ipv6, etc.)
    let distant_ipv4 = Ipv4Addr::from_str(kernel_ipv4)?; // Ipv4Addr::new(192, 168, 222, 1);
    let local_ipv4 = Ipv4Addr::from_str(client_ipv4)?; // Ipv4Addr::new(192, 168, 222, 2);
    let local_mac = ethernet::Address::from_str(client_mac)?;

    // TODO(address): must be computed in IPv6 layer
    let local_ipv6 = Ipv6Addr::from_str(client_ipv6)?; // Ipv6Addr::new(0xfe80, 0, 0, 0, 0, 0, 0, 0x0022);

    // Create physical interface
    let mut tap_iface = tuntap::Interface::new_tap();
    tap_iface.open()?;
    info!("Iface name: {}", tap_iface.name());
    ip_command::set_ip_address(tap_iface.name(), &IpAddr::V4(distant_ipv4), 24)?;

    // FIXME The linux seems to ignore NDP NA.  Try to find why and fix it.
    // The fix right now is to manually insert the entry in the ARP table.
    ip_command::set_neighbor_ip_address(tap_iface.name(), &IpAddr::V6(local_ipv6), &local_mac)?;

    let ethernet_process = build_l2_process(local_mac, local_ipv4, local_ipv6, true, false)?;

    // Build l1 process
    let tuntap_process = tuntap_process::Process::new(Box::new(ethernet_process));

    // Main Loop
    info!("Starting main loop");
    let mut interface_loop = interface_loop::HighLevelInterface::new(Box::new(tap_iface), &system);
    interface_loop.start(Box::new(tuntap_process))?;
    Ok(interface_loop)
}

enum MainControl {
    SignalCtrlc,
    // XXX add other main control here.
}

fn main_loop(
    handler: &mut interface_loop::HighLevelInterface,
    self_kill: Option<usize>,
) -> Result<(), Box<dyn Error>> {
    // Managing signal (sigint, sigterm, sighup)
    let (sender, receiver) = crossbeam_channel::bounded::<MainControl>(1);

    if let Some(timeout) = self_kill {
        let self_kill_sender = sender.clone();
        thread::spawn(move || {
            thread::sleep(time::Duration::from_secs(timeout as u64));
            if let Err(err) = self_kill_sender.send(MainControl::SignalCtrlc) {
                error!("Failed to send self kill instruction to main: {}", err);
            }
        });
    }

    if let Err(err) = ctrlc::set_handler(move || {
        if let Err(err) = sender.send(MainControl::SignalCtrlc) {
            error!("Failed to send signal to main: {}", err);
        }
    }) {
        warn!("Failed to set sigint handler: {}", err);
    }

    loop {
        match receiver.recv() {
            Err(err) => {
                error!("error while receiving main insruction: {}", err);
            }
            Ok(value) => match value {
                MainControl::SignalCtrlc => {
                    info!("stopping main loop...");
                    handler.stop()?;
                    handler.join();
                }
            },
        }
        break;
    }

    Ok(())
}

fn main() {
    // Processing arguments
    let args = Args::parse();

    // Initialising the logger
    let log_level;
    if args.debug {
        log_level = LevelFilter::Debug;
    } else {
        log_level = LevelFilter::Info;
    }
    env_logger::Builder::new().filter_level(log_level).init();

    // Init the App
    let system = layers::ProcessSystem::new(2);
    let mut main_handler = match init_interface(
        &args.mac,
        &args.client_ipv4,
        &args.client_ipv6,
        &args.kernel_ipv4,
        &system,
    ) {
        Err(error) => {
            error!("{}", error);
            return;
        }
        Ok(handler) => handler,
    };

    // Start the main loop
    if let Err(err) = main_loop(&mut main_handler, args.self_kill) {
        error!("{}", err);
    }
    debug!("Exiting...");
}
