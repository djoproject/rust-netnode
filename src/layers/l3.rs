use crate::layers;
use crate::layers::l2;
use crate::utils::result;

pub mod icmpv4;
pub mod icmpv6;
pub mod ipv4;
pub mod ipv6;

pub trait Layer: layers::Layer {
    fn add_address(&mut self, _address: &[u8]) -> result::Result<()> {
        return Err(result::Error::new(
            result::ErrorKind::MethodNotSupportedForThisLayer("l3", "add_address"),
        ));
    }

    fn update_lower_layer(&mut self, _l2: &mut dyn l2::Layer) -> result::Result<()> {
        Ok(())
    }
}
