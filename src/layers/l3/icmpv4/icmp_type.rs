use std::fmt;

const ECHO_REPLY: u8 = 0x00_u8;
const DESTINATION_UNREACHABLE: u8 = 0x03_u8;
const SOURCE_QUENCH: u8 = 0x04_u8;
const REDIRECT_MESSAGE: u8 = 0x05_u8;
const ALTERNATE_HOST_ADDRESS: u8 = 0x06_u8; // Deprecated
const ECHO_REQUEST: u8 = 0x08_u8;
const ROUTER_ADVERTISEMENT: u8 = 0x09_u8;
const ROUTER_SOLICITATION: u8 = 0x0A_u8;
const TIME_EXCEEDED: u8 = 0x0B_u8;
const PARAMETER_PROBLEM: u8 = 0x0C_u8;
const TIMESTAMP: u8 = 0x0D_u8;
const TIMESTAMP_REPLY: u8 = 0x0E_u8;
const INFORMATION_REQUEST: u8 = 0x0F_u8; // Deprecated
const INFORMATION_REPLY: u8 = 0x10_u8; // Deprecated
const ADDRESS_MASK_REQUEST: u8 = 0x11_u8; // Deprecated
const ADDRESS_MASK_REPLY: u8 = 0x12_u8; // Deprecated
const RESERVED1: u8 = 0x13_u8;
const RESERVED2: u8 = 0x14_u8;
const RESERVED3: u8 = 0x15_u8;
const RESERVED4: u8 = 0x16_u8;
const RESERVED5: u8 = 0x17_u8;
const RESERVED6: u8 = 0x18_u8;
const RESERVED7: u8 = 0x19_u8;
const RESERVED8: u8 = 0x1A_u8;
const RESERVED9: u8 = 0x1B_u8;
const RESERVED10: u8 = 0x1C_u8;
const RESERVED11: u8 = 0x1D_u8;
const TRACEROUTE: u8 = 0x1E_u8; // Deprecated
const DATAGRAM_CONVERSION_ERROR: u8 = 0x1F_u8; // Deprecated
const MOBILE_HOST_REDIRECT: u8 = 0x20_u8; // Deprecated
const WHERE_ARE_YOU: u8 = 0x21_u8; // Deprecated
const HERE_I_AM: u8 = 0x22_u8; // Deprecated
const MOBILE_REGISTRATION_REQUEST: u8 = 0x23_u8; // Deprecated
const MOBILE_REGISTRATION_REPLY: u8 = 0x24_u8; // Deprecated
const DOMAIN_NAME_REQUEST: u8 = 0x25_u8; // Deprecated
const DOMAIN_NAME_REPLY: u8 = 0x26_u8; // Deprecated
const SKIP: u8 = 0x27_u8; // Deprecated
const PHOTURIS: u8 = 0x28_u8;
const ICMP_MOBILITY: u8 = 0x29_u8;
const EXTENDED_ECHO_REQUEST: u8 = 0x2A_u8;
const EXTENDED_ECHO_REPLY: u8 = 0x2B_u8;
const EXPERIMENTAL1: u8 = 0xFD_u8;
const EXPERIMENTAL2: u8 = 0xFE_u8;
const RESERVED12: u8 = 0xFF_u8;

#[repr(u8)]
#[derive(PartialEq, Eq, Debug, Copy, Clone, Hash)]
pub enum IcmpType {
    EchoReply = ECHO_REPLY,
    DestinationUnreachable = DESTINATION_UNREACHABLE,
    SourceQuench = SOURCE_QUENCH,
    RedirectMessage = REDIRECT_MESSAGE,
    AlternateHostAddress = ALTERNATE_HOST_ADDRESS,
    EchoRequest = ECHO_REQUEST,
    RouterAdvertisement = ROUTER_ADVERTISEMENT,
    RouterSolicitation = ROUTER_SOLICITATION,
    TimeExceeded = TIME_EXCEEDED,
    ParameterProblem = PARAMETER_PROBLEM,
    Timestamp = TIMESTAMP,
    TimestampReply = TIMESTAMP_REPLY,
    InformationRequest = INFORMATION_REQUEST,
    InformationReply = INFORMATION_REPLY,
    AddressMaskRequest = ADDRESS_MASK_REQUEST,
    AddressMaskReply = ADDRESS_MASK_REPLY,
    Reserved1 = RESERVED1,
    Reserved2 = RESERVED2,
    Reserved3 = RESERVED3,
    Reserved4 = RESERVED4,
    Reserved5 = RESERVED5,
    Reserved6 = RESERVED6,
    Reserved7 = RESERVED7,
    Reserved8 = RESERVED8,
    Reserved9 = RESERVED9,
    Reserved10 = RESERVED10,
    Reserved11 = RESERVED11,
    Traceroute = TRACEROUTE,
    DatagramConversionError = DATAGRAM_CONVERSION_ERROR,
    MobileHostRedirect = MOBILE_HOST_REDIRECT,
    WhereAreYou = WHERE_ARE_YOU,
    HereIAm = HERE_I_AM,
    MobileRegistrationRequest = MOBILE_REGISTRATION_REQUEST,
    MobileRegistrationReply = MOBILE_REGISTRATION_REPLY,
    DomainNameRequest = DOMAIN_NAME_REQUEST,
    DomainNameReply = DOMAIN_NAME_REPLY,
    Skip = SKIP,
    Photuris = PHOTURIS,
    IcmpMobility = ICMP_MOBILITY,
    ExtendedEchoRequest = EXTENDED_ECHO_REQUEST,
    ExtendedEchoReply = EXTENDED_ECHO_REPLY,
    Experimental1 = EXPERIMENTAL1,
    Experimental2 = EXPERIMENTAL2,
    Reserved12 = RESERVED12,
}

impl IcmpType {
    pub fn is_deprecated(&self) -> bool {
        match self {
            Self::AlternateHostAddress => true,
            Self::InformationRequest => true,
            Self::InformationReply => true,
            Self::AddressMaskRequest => true,
            Self::AddressMaskReply => true,
            Self::Traceroute => true,
            Self::DatagramConversionError => true,
            Self::MobileHostRedirect => true,
            Self::WhereAreYou => true,
            Self::HereIAm => true,
            Self::MobileRegistrationRequest => true,
            Self::MobileRegistrationReply => true,
            Self::DomainNameRequest => true,
            Self::DomainNameReply => true,
            Self::Skip => true,
            _ => false,
        }
    }
}

impl fmt::Display for IcmpType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::EchoReply => "Echo reply",
                Self::DestinationUnreachable => "Destination Unreachable",
                Self::SourceQuench => "Source Quench",
                Self::RedirectMessage => "Redirect Message",
                Self::AlternateHostAddress => "Alternate Host Address",
                Self::EchoRequest => "Echo request",
                Self::RouterAdvertisement => "Router Advertisement",
                Self::RouterSolicitation => "Router discovery/selection/solicitation",
                Self::TimeExceeded => "Time Exceeded",
                Self::ParameterProblem => "Parameter Problem: Bad IP header",
                Self::Timestamp => "Timestamp",
                Self::TimestampReply => "Timestamp reply",
                Self::InformationRequest => "Information Request",
                Self::InformationReply => "Information Reply",
                Self::AddressMaskRequest => "Address Mask Request",
                Self::AddressMaskReply => "Address Mask Reply",
                Self::Reserved1 => "Reserved for security",
                Self::Reserved2 => "Reserved for robustness experiment",
                Self::Reserved3 => "Reserved for robustness experiment",
                Self::Reserved4 => "Reserved for robustness experiment",
                Self::Reserved5 => "Reserved for robustness experiment",
                Self::Reserved6 => "Reserved for robustness experiment",
                Self::Reserved7 => "Reserved for robustness experiment",
                Self::Reserved8 => "Reserved for robustness experiment",
                Self::Reserved9 => "Reserved for robustness experiment",
                Self::Reserved10 => "Reserved for robustness experiment",
                Self::Reserved11 => "Reserved for robustness experiment",
                Self::Traceroute => "Traceroute",
                Self::DatagramConversionError => "Datagram Conversion Error",
                Self::MobileHostRedirect => "Mobile Host Redirect",
                Self::WhereAreYou => "Where-Are-You",
                Self::HereIAm => "Here-I-Am",
                Self::MobileRegistrationRequest => "Mobile Registration Request ",
                Self::MobileRegistrationReply => "Mobile Registration Reply",
                Self::DomainNameRequest => "Domain Name Request",
                Self::DomainNameReply => "Domain Name Reply",
                Self::Skip => "SKIP Algorithm Discovery Protocol",
                Self::Photuris => "Photuris, Security failures",
                Self::IcmpMobility => "ICMP for experimental mobility protocols",
                Self::ExtendedEchoRequest => "Extended Echo Request",
                Self::ExtendedEchoReply => "Extended Echo Reply",
                Self::Experimental1 => "RFC3692-style Experiment 1",
                Self::Experimental2 => "RFC3692-style Experiment 2",
                Self::Reserved12 => "Reserved",
            }
        )
    }
}

impl From<u8> for IcmpType {
    fn from(value: u8) -> Self {
        match value {
            ECHO_REPLY => IcmpType::EchoReply,
            DESTINATION_UNREACHABLE => IcmpType::DestinationUnreachable,
            SOURCE_QUENCH => IcmpType::SourceQuench,
            REDIRECT_MESSAGE => IcmpType::RedirectMessage,
            ALTERNATE_HOST_ADDRESS => IcmpType::AlternateHostAddress,
            ECHO_REQUEST => IcmpType::EchoRequest,
            ROUTER_ADVERTISEMENT => IcmpType::RouterAdvertisement,
            ROUTER_SOLICITATION => IcmpType::RouterSolicitation,
            TIME_EXCEEDED => IcmpType::TimeExceeded,
            PARAMETER_PROBLEM => IcmpType::ParameterProblem,
            TIMESTAMP => IcmpType::Timestamp,
            TIMESTAMP_REPLY => IcmpType::TimestampReply,
            INFORMATION_REQUEST => IcmpType::InformationRequest,
            INFORMATION_REPLY => IcmpType::InformationReply,
            ADDRESS_MASK_REQUEST => IcmpType::AddressMaskRequest,
            ADDRESS_MASK_REPLY => IcmpType::AddressMaskReply,
            RESERVED1 => IcmpType::Reserved1,
            RESERVED2 => IcmpType::Reserved2,
            RESERVED3 => IcmpType::Reserved3,
            RESERVED4 => IcmpType::Reserved4,
            RESERVED5 => IcmpType::Reserved5,
            RESERVED6 => IcmpType::Reserved6,
            RESERVED7 => IcmpType::Reserved7,
            RESERVED8 => IcmpType::Reserved8,
            RESERVED9 => IcmpType::Reserved9,
            RESERVED10 => IcmpType::Reserved10,
            RESERVED11 => IcmpType::Reserved11,
            TRACEROUTE => IcmpType::Traceroute,
            DATAGRAM_CONVERSION_ERROR => IcmpType::DatagramConversionError,
            MOBILE_HOST_REDIRECT => IcmpType::MobileHostRedirect,
            WHERE_ARE_YOU => IcmpType::WhereAreYou,
            HERE_I_AM => IcmpType::HereIAm,
            MOBILE_REGISTRATION_REQUEST => IcmpType::MobileRegistrationRequest,
            MOBILE_REGISTRATION_REPLY => IcmpType::MobileRegistrationReply,
            DOMAIN_NAME_REQUEST => IcmpType::DomainNameRequest,
            DOMAIN_NAME_REPLY => IcmpType::DomainNameReply,
            SKIP => IcmpType::Skip,
            PHOTURIS => IcmpType::Photuris,
            ICMP_MOBILITY => IcmpType::IcmpMobility,
            EXTENDED_ECHO_REQUEST => IcmpType::ExtendedEchoRequest,
            EXTENDED_ECHO_REPLY => IcmpType::ExtendedEchoReply,
            EXPERIMENTAL1 => IcmpType::Experimental1,
            EXPERIMENTAL2 => IcmpType::Experimental2,
            RESERVED12 => IcmpType::Reserved12,
            _ => IcmpType::Reserved12,
        }
    }
}

impl From<IcmpType> for u8 {
    fn from(value: IcmpType) -> u8 {
        value as u8
    }
}
