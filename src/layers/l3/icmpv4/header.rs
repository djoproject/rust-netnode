use crate::layers;
use crate::layers::l3::icmpv4::IcmpType;

use std::fmt;

//    0               1               2               3
//    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//   |     Type      |     Code      |          Checksum             |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//   |                             unused                            |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//   |      Internet Header + 64 bits of Original Data Datagram      |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct Header {
    pub type_: IcmpType, // TODO(enum) FIXME what would happen if it receive a value not managed by the enum ? Check other headers
    pub code: u8,
    pub checksum: u16,
}

impl Header {
    pub fn init(&mut self, type_: IcmpType, code: u8) {
        self.type_ = type_;
        self.code = code;
        self.checksum = 0x0;
    }
}

impl fmt::Display for Header {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "Icmpv4, code: {}, type: {:#04x}, checksum: {:#06x}",
            self.type_, self.code, self.checksum
        )
    }
}

impl layers::Header for Header {}
