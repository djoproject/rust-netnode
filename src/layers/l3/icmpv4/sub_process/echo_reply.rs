use crate::layers;
use crate::layers::l3;
use crate::utils::buffer;

pub struct EchoReply;

impl EchoReply {
    pub fn new() -> Self {
        Self {}
    }
}

impl l3::Layer for EchoReply {}

impl layers::Layer for EchoReply {
    fn get_meta_type(&self) -> layers::ProcessMetaType {
        return layers::ProcessMetaType::ICMPv4;
    }

    fn process<'a>(
        &self,
        _system: &'a mut layers::ProcessSystem,
        _buffer: buffer::Buffer,
        _meta: layers::ProcessMeta,
    ) -> (Option<layers::ProcessResult>, Option<buffer::Buffer>) {
        // XXX won't do a thing for now
        (None, None)
    }
}
