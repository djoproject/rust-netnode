use crate::layers;
use crate::layers::l3;
use crate::layers::l3::icmpv4::sub_process;
use crate::layers::l3::icmpv4::Header;
use crate::layers::l3::icmpv4::IcmpType;
use crate::layers::l3::ipv4;
use crate::utils::buffer;
use crate::utils::result;

use log::warn;

use std::collections::HashMap;

pub struct Process {
    checksum_enabled: bool,
    code_type_subprocess: HashMap<IcmpType, Box<dyn layers::Layer>>,
}

impl Process {
    pub fn new(checksum_enabled: bool) -> Self {
        return Self {
            checksum_enabled,
            code_type_subprocess: HashMap::new(),
        };
    }

    pub fn enable_type(&mut self, icmp_type: IcmpType) -> result::Result<()> {
        match icmp_type {
            IcmpType::EchoReply => {
                self.code_type_subprocess
                    .insert(icmp_type, Box::new(sub_process::EchoReply::new()));
                Ok(())
            }
            IcmpType::EchoRequest => {
                self.code_type_subprocess
                    .insert(icmp_type, Box::new(sub_process::EchoRequest::new()));
                Ok(())
            }
            _ => {
                return Err(result::Error::new(
                    result::ErrorKind::UnsupportedIcmpV4Type(icmp_type),
                ))
            }
        }
    }
}

impl l3::Layer for Process {}

impl layers::Layer for Process {
    fn get_meta_type(&self) -> layers::ProcessMetaType {
        return layers::ProcessMetaType::ICMPv4;
    }

    fn process<'a>(
        &self,
        system: &'a mut layers::ProcessSystem,
        buffer: buffer::Buffer,
        meta: layers::ProcessMeta,
    ) -> (Option<layers::ProcessResult>, Option<buffer::Buffer>) {
        // Check length
        let icmpv4_min_size = meta.start_at + 4;
        if buffer.len() < icmpv4_min_size {
            warn!(
                "Datagram too small to contains icmpV4, expected {}, got {}",
                icmpv4_min_size,
                buffer.len()
            );
            return (None, None);
        }

        // Check checksum
        if self.checksum_enabled {
            let mut checksum = ipv4::Checksum::new();
            checksum.add_data(&buffer[meta.start_at..]);

            if !checksum.verify() {
                warn!("invalid checksum, packet dropped.");
                return (None, None);
            }
        }

        // Process datagram
        let icmp_hdr: &Header = buffer.to_header(meta.start_at);

        if let Some(process) = self.code_type_subprocess.get(&icmp_hdr.type_) {
            let (_, buffer) = process.process(system, buffer, meta);
            return (None, buffer);
        }

        (None, None)
    }
}
