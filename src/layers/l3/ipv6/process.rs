use crate::layers;
use crate::layers::l2;
use crate::layers::l2::ethernet;
use crate::layers::l3;
use crate::layers::l3::ipv4;
use crate::layers::l3::ipv6::Header;
use crate::layers::Protocol;
use crate::utils::buffer;
use crate::utils::result;

use log::warn;

use std::collections::HashMap;
use std::collections::HashSet;
use std::net::Ipv6Addr;

pub struct Process {
    protocol_subprocess: HashMap<ipv4::Protocol, Box<dyn layers::Layer>>,
    main_address: Ipv6Addr,
    addresses: HashSet<[u8; 16]>,
    dropped_subprocess: Option<Box<dyn layers::Layer>>,
}

impl Process {
    // TODO(address): same is in IPv4, adding an address needs the l2 access
    pub fn new(address: Ipv6Addr) -> Self {
        let mut process = Process {
            main_address: address,
            protocol_subprocess: HashMap::new(),
            addresses: HashSet::new(),
            dropped_subprocess: None,
        };

        process.addresses.insert(address.octets());
        process
    }

    pub fn add_protocol_process(
        &mut self,
        protocol: ipv4::Protocol,
        process: Box<dyn layers::Layer>,
    ) {
        self.protocol_subprocess.insert(protocol, process);
    }

    pub fn set_dropped_process(&mut self, process: Box<dyn layers::Layer>) {
        self.dropped_subprocess = Some(process);
    }
}

impl l3::Layer for Process {
    fn add_address(&mut self, address: &[u8]) -> result::Result<()> {
        if address.len() != 16 {
            return Err(result::Error::new(result::ErrorKind::WrongLengthAddress(
                16,
                address.len(),
            )));
        }

        // if its an unicast address, compute and insert its Solicited-node multicast address
        if address[0] != 0xff {
            let mut addr: [u8; 16] = [0xff, 0x02, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x01, 0xff, 0, 0, 0];
            addr[13] = address[13];
            addr[14] = address[14];
            addr[15] = address[15];
            self.add_address(addr.as_slice())?;
        }

        self.addresses.insert(address[0..16].try_into().unwrap());
        Ok(())
    }

    fn update_lower_layer(&mut self, l2: &mut dyn l2::Layer) -> result::Result<()> {
        // Only the ethernet layer is managed
        let l2_meta_type = l2.get_meta_type();
        if l2_meta_type != layers::ProcessMetaType::Ethernet {
            return Err(result::Error::new(
                result::ErrorKind::UnsuportedProcessMetaType(l2_meta_type),
            ));
        }

        // Add MAC ipv6 All host
        l2.add_address(ethernet::DefaultAddress::IPv6MulticastAllHosts.octets())?;

        for ipv6_addr in &self.addresses {
            if ipv6_addr[0] != 0xff {
                continue;
            }

            let mut mac_addr: [u8; 6] = [0x33, 0x33, 0, 0, 0, 0];
            for index in 2..6 {
                mac_addr[index] = ipv6_addr[index + 10];
            }
            l2.add_address(&mac_addr)?;
        }

        Ok(())
    }
}

impl layers::Layer for Process {
    fn get_meta_type(&self) -> layers::ProcessMetaType {
        return layers::ProcessMetaType::IPv6;
    }

    fn process<'a>(
        &self,
        system: &'a mut layers::ProcessSystem,
        buffer: buffer::Buffer,
        meta: layers::ProcessMeta,
    ) -> (Option<layers::ProcessResult>, Option<buffer::Buffer>) {
        let ipv6_min_size = meta.start_at + 40;
        if buffer.len() < ipv6_min_size {
            warn!(
                "Packet too small to contains ipv6, expected {}, got {}",
                ipv6_min_size,
                buffer.len()
            );
            return (None, None);
        }

        let ipv6_hdr: &Header = buffer.to_header(meta.start_at);
        let build_next_meta = || layers::ProcessMeta::from_meta(&meta, 40, self.get_meta_type());

        // Does this process manage this address ?
        if !self.addresses.contains(&ipv6_hdr.dst_addr) {
            if let Some(process) = &self.dropped_subprocess {
                process.process(system, buffer, build_next_meta());
            } else {
            }
            return (None, None);
        }

        // Does it manage the carried protocol ?
        let result: Option<buffer::Buffer>;
        let next_header = ipv6_hdr.next_header;
        let dst_addr = ipv6_hdr.src_addr;

        if let Some(process) = self.protocol_subprocess.get(&ipv6_hdr.next_header) {
            let meta =
                layers::ProcessMeta::from_meta(&meta, 40, ipv6_hdr.next_header.get_meta_type());
            (_, result) = process.process(system, buffer, meta);
        } else {
            if let Some(process) = &self.dropped_subprocess {
                process.process(system, buffer, build_next_meta());
            } else {
            }
            return (None, None);
        }

        // Is there an answer to return ?
        if let Some(mut answer) = result {
            let payload_length: u16 = (answer.len() - meta.answer_at - 40) as u16;
            let ipv6_hdr_answer: &mut Header = answer.to_header_mut(meta.answer_at);

            // Initialize header
            // TODO(address) should not have a main_address but a system to compute the best suitable src
            // address from the address pool
            ipv6_hdr_answer.init(payload_length, next_header);
            ipv6_hdr_answer.src_addr = self.main_address.octets();
            ipv6_hdr_answer.dst_addr = dst_addr;

            return (None, Some(answer));
        }
        (None, None)
    }
}
