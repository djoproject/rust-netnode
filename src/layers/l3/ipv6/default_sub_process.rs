use crate::layers;
use crate::layers::l3;
use crate::layers::l3::ipv6::Header;
use crate::utils::buffer;

use log::info;

pub struct DefaultSubProcess<'a> {
    prefix: &'a str,
}

impl<'a> DefaultSubProcess<'a> {
    pub fn new(prefix: &'a str) -> Self {
        return DefaultSubProcess { prefix };
    }
}

impl l3::Layer for DefaultSubProcess<'_> {}

impl layers::Layer for DefaultSubProcess<'_> {
    fn get_meta_type(&self) -> layers::ProcessMetaType {
        return layers::ProcessMetaType::UnManaged;
    }

    fn process<'a>(
        &self,
        _system: &'a mut layers::ProcessSystem,
        buffer: buffer::Buffer,
        meta: layers::ProcessMeta,
    ) -> (Option<layers::ProcessResult>, Option<buffer::Buffer>) {
        let header: &Header = buffer.to_header(meta.start_at - 40);
        info!("{} {}", self.prefix, header);
        (None, None)
    }
}
