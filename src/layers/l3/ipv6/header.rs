use crate::layers::l3::ipv4;

use std::fmt;
use std::net::Ipv6Addr;

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct Header {
    pub version_class_flowlabel: u32,
    pub payload_length: u16,
    pub next_header: ipv4::Protocol,
    pub hop_limit: u8,
    pub src_addr: [u8; 16], // XXX can't directly use Ipv6Addr struct here, they are not repr(C)
    pub dst_addr: [u8; 16],
}

impl Header {
    pub fn init(&mut self, payload_length: u16, next_header: ipv4::Protocol) {
        self.version_class_flowlabel = 0x60000000_u32.to_be();
        self.payload_length = payload_length.to_be();
        self.next_header = next_header;
        self.hop_limit = 0xfe;
    }

    pub fn get_src(&self) -> Ipv6Addr {
        Ipv6Addr::new(
            ((self.src_addr[0] as u16) << 8) + self.src_addr[1] as u16,
            ((self.src_addr[2] as u16) << 8) + self.src_addr[3] as u16,
            ((self.src_addr[4] as u16) << 8) + self.src_addr[5] as u16,
            ((self.src_addr[6] as u16) << 8) + self.src_addr[7] as u16,
            ((self.src_addr[8] as u16) << 8) + self.src_addr[9] as u16,
            ((self.src_addr[10] as u16) << 8) + self.src_addr[11] as u16,
            ((self.src_addr[12] as u16) << 8) + self.src_addr[13] as u16,
            ((self.src_addr[14] as u16) << 8) + self.src_addr[15] as u16,
        )
    }
    pub fn get_dst(&self) -> Ipv6Addr {
        Ipv6Addr::new(
            ((self.dst_addr[0] as u16) << 8) + self.dst_addr[1] as u16,
            ((self.dst_addr[2] as u16) << 8) + self.dst_addr[3] as u16,
            ((self.dst_addr[4] as u16) << 8) + self.dst_addr[5] as u16,
            ((self.dst_addr[6] as u16) << 8) + self.dst_addr[7] as u16,
            ((self.dst_addr[8] as u16) << 8) + self.dst_addr[9] as u16,
            ((self.dst_addr[10] as u16) << 8) + self.dst_addr[11] as u16,
            ((self.dst_addr[12] as u16) << 8) + self.dst_addr[13] as u16,
            ((self.dst_addr[14] as u16) << 8) + self.dst_addr[15] as u16,
        )
    }
}

impl fmt::Display for Header {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "src: {}, dst: {}, protocol: {}",
            self.get_src(),
            self.get_dst(),
            self.next_header
        )
    }
}
