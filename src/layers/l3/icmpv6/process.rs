use crate::layers;
use crate::layers::l2::ethernet;
use crate::layers::l3;
use crate::layers::l3::icmpv6::sub_process;
use crate::layers::l3::icmpv6::Header;
use crate::layers::l3::icmpv6::IcmpType;
use crate::layers::l3::ipv4;
use crate::layers::l3::ipv6;
use crate::utils::buffer;
use crate::utils::result;

use log::warn;

use std::collections::HashMap;

pub struct Process {
    checksum_enabled: bool,
    type_subprocess: HashMap<IcmpType, Box<dyn layers::Layer>>,
    mac: ethernet::Address,
}

impl Process {
    pub fn new(checksum_enabled: bool, mac: ethernet::Address) -> Self {
        return Self {
            checksum_enabled,
            type_subprocess: HashMap::new(),
            mac,
        };
    }

    pub fn enable_type(&mut self, icmp_type: IcmpType) -> result::Result<()> {
        match icmp_type {
            IcmpType::EchoRequest => {
                self.type_subprocess
                    .insert(icmp_type, Box::new(sub_process::EchoRequest::new()));
                Ok(())
            }
            IcmpType::EchoReply => {
                self.type_subprocess
                    .insert(icmp_type, Box::new(sub_process::EchoReply::new()));
                Ok(())
            }
            IcmpType::NeighborSolicitation => {
                self.type_subprocess.insert(
                    icmp_type,
                    Box::new(sub_process::NeighborSolicitation::new(self.mac)),
                );
                Ok(())
            }
            IcmpType::NeighborAdvertisement => {
                self.type_subprocess.insert(
                    icmp_type,
                    Box::new(sub_process::NeighborAdvertisement::new()),
                );
                Ok(())
            }
            _ => {
                return Err(result::Error::new(
                    result::ErrorKind::UnsupportedIcmpV6Type(icmp_type),
                ))
            }
        }
    }
}

impl l3::Layer for Process {}

impl layers::Layer for Process {
    fn get_meta_type(&self) -> layers::ProcessMetaType {
        return layers::ProcessMetaType::ICMPv6;
    }

    fn process<'a>(
        &self,
        system: &'a mut layers::ProcessSystem,
        buffer: buffer::Buffer,
        meta: layers::ProcessMeta,
    ) -> (Option<layers::ProcessResult>, Option<buffer::Buffer>) {
        // Check length
        let icmpv6_min_size = meta.start_at + 4;
        if buffer.len() < icmpv6_min_size {
            warn!(
                "Datagram too small to contains icmpV6, expected {}, got {}",
                icmpv6_min_size,
                buffer.len()
            );
            return (None, None);
        }

        // Check checksum
        if self.checksum_enabled {
            let mut checksum = ipv4::Checksum::new();
            let ipv6_hdr: &ipv6::Header = buffer.to_header(meta.previous.unwrap().start_at);

            // add the pseudoheader to checksum
            checksum.add_data(&ipv6_hdr.src_addr);
            checksum.add_data(&ipv6_hdr.dst_addr);
            checksum.add_data(&((ipv6_hdr.payload_length.to_be()) as u32).to_be_bytes());
            checksum.add_data(&[0, 0, 0, ipv6_hdr.next_header.into()]);

            // add icmp header + payload to checksum
            checksum.add_data(&buffer[meta.start_at..]);

            if !checksum.verify() {
                warn!("invalid checksum, packet dropped.");
                return (None, None);
            }
        }

        // Process datagram
        let icmp_hdr: &Header = buffer.to_header(meta.start_at);
        if let Some(process) = self.type_subprocess.get(&icmp_hdr.type_) {
            let (_, answer) = process.process(system, buffer, meta);
            return (None, answer);
        }
        (None, None)
    }
}
