use std::fmt;

const DESTINATION_UNREACHABLE: u8 = 0x01_u8;
const PACKET_TOO_BIG: u8 = 0x02_u8;
const TIME_EXCEEDED: u8 = 0x03_u8;
const PARAMETER_PROBLEM: u8 = 0x04_u8;
const PRIVATE_EXPERIMENTATION1: u8 = 0x64_u8;
const PRIVATE_EXPERIMENTATION2: u8 = 0x65_u8;
const RESERVED1: u8 = 0x7f_u8;
const ECHO_REQUEST: u8 = 0x80_u8;
const ECHO_REPLY: u8 = 0x81_u8;
const MULTICAST_LISTENER_QUERY: u8 = 0x82_u8;
const MULTICAST_LISTENER_REPORT: u8 = 0x83_u8;
const MULTICAST_LISTENER_DONE: u8 = 0x84_u8;
const ROUTER_SOLICITATION: u8 = 0x85_u8;
const ROUTER_ADVERTISEMENT: u8 = 0x86_u8;
const NEIGHBOR_SOLICITATION: u8 = 0x87_u8;
const NEIGHBOR_ADVERTISEMENT: u8 = 0x88_u8;
const REDIRECT_MESSAGE: u8 = 0x89_u8;
const ROUTER_RENUMBERING: u8 = 0x8a_u8;
const ICMP_NODE_INFORMATION_QUERY: u8 = 0x8b_u8;
const ICMP_NODE_INFORMATION_RESPONSE: u8 = 0x8c_u8;
const INVERSE_NEIGHBOR_DISCOVERY_SOLICITATION: u8 = 0x8d_u8;
const INVERSE_NEIGHBOR_DISCOVERY_ADVERTISEMENT: u8 = 0x8e_u8;
const MULTICAST2_LISTENER_DISCOVERY_REPORTS: u8 = 0x8f_u8;
const HOME_AGENT_ADDRESS_DISCOVRY_REQUEST: u8 = 0x90_u8;
const HOME_AGENT_ADDRESS_DISCOVRY_REPLY: u8 = 0x91_u8;
const MOBILE_PREFIX_SOLICITATION: u8 = 0x92_u8;
const MOBILE_PREFIX_ADVERTISEMENT: u8 = 0x93_u8;
const CERTIFICATION_PATH_SOLICITATION: u8 = 0x94_u8;
const CERTIFICATION_PATH_ADVERTISEMENT: u8 = 0x95_u8;
const MULTICAST_ROUTER_ADVERTISEMENT: u8 = 0x97_u8;
const MULTICAST_ROUTER_SOLICITATION: u8 = 0x98_u8;
const MULTICAST_ROUTER_TERMINATION: u8 = 0x99_u8;
const RPL_CONTROL: u8 = 0x9b_u8;
const PRIVATE_EXPERIMENTATION3: u8 = 0xc8_u8;
const PRIVATE_EXPERIMENTATION4: u8 = 0xc9_u8;
const RESERVED2: u8 = 0xff_u8;

#[repr(u8)]
#[derive(PartialEq, Eq, Debug, Copy, Clone, Hash)]
pub enum IcmpType {
    DestinationUnreachable = DESTINATION_UNREACHABLE,
    PacketTooBig = PACKET_TOO_BIG,
    TimeExceeded = TIME_EXCEEDED,
    ParameterProblem = PARAMETER_PROBLEM,
    PrivateExperimentation1 = PRIVATE_EXPERIMENTATION1,
    PrivateExperimentation2 = PRIVATE_EXPERIMENTATION2,
    Reserved1 = RESERVED1,
    EchoRequest = ECHO_REQUEST,
    EchoReply = ECHO_REPLY,
    MulticastListenerQuery = MULTICAST_LISTENER_QUERY,
    MulticastListenerReport = MULTICAST_LISTENER_REPORT,
    MulticastListenerDone = MULTICAST_LISTENER_DONE,
    RouterSolicitation = ROUTER_SOLICITATION,
    RouterAdvertisement = ROUTER_ADVERTISEMENT,
    NeighborSolicitation = NEIGHBOR_SOLICITATION,
    NeighborAdvertisement = NEIGHBOR_ADVERTISEMENT,
    RedirectMessage = REDIRECT_MESSAGE,
    RouterRenumbering = ROUTER_RENUMBERING,
    IcmpNodeInformationQuery = ICMP_NODE_INFORMATION_QUERY,
    IcmpNodeInformationResponse = ICMP_NODE_INFORMATION_RESPONSE,
    InverseNeighborDiscoverySolicitation = INVERSE_NEIGHBOR_DISCOVERY_SOLICITATION,
    InverseNeighborDiscoveryAdvertisement = INVERSE_NEIGHBOR_DISCOVERY_ADVERTISEMENT,
    Multicast2ListenerDiscoveryReports = MULTICAST2_LISTENER_DISCOVERY_REPORTS,
    HomeAgentAddressDiscovryRequest = HOME_AGENT_ADDRESS_DISCOVRY_REQUEST,
    HomeAgentAddressDiscovryReply = HOME_AGENT_ADDRESS_DISCOVRY_REPLY,
    MobilePrefixSolicitation = MOBILE_PREFIX_SOLICITATION,
    MobilePrefixAdvertisement = MOBILE_PREFIX_ADVERTISEMENT,
    CertificationPathSolicitation = CERTIFICATION_PATH_SOLICITATION,
    CertificationPathAdvertisement = CERTIFICATION_PATH_ADVERTISEMENT,
    MulticastRouterAdvertisement = MULTICAST_ROUTER_ADVERTISEMENT,
    MulticastRouterSolicitation = MULTICAST_ROUTER_SOLICITATION,
    MulticastRouterTermination = MULTICAST_ROUTER_TERMINATION,
    RplControl = RPL_CONTROL,
    PrivateExperimentation3 = PRIVATE_EXPERIMENTATION3,
    PrivateExperimentation4 = PRIVATE_EXPERIMENTATION4,
    Reserved2 = RESERVED2,
}

impl IcmpType {
    pub fn is_deprecated(&self) -> bool {
        match self {
            _ => false,
        }
    }
}

impl fmt::Display for IcmpType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::DestinationUnreachable => "Destination unreachable",
                Self::PacketTooBig => "Packet too big",
                Self::TimeExceeded => "Time exceeded",
                Self::ParameterProblem => "Parameter problem",
                Self::PrivateExperimentation1 => "Private experimentation",
                Self::PrivateExperimentation2 => "Private experimentation",
                Self::Reserved1 => "Reserved for expansion of ICMPv6 error messages",
                Self::EchoRequest => "Echo Request",
                Self::EchoReply => "Echo Reply",
                Self::MulticastListenerQuery => "Multicast Listener Query",
                Self::MulticastListenerReport => "Multicast Listener Report",
                Self::MulticastListenerDone => "Multicast Listener Done",
                Self::RouterSolicitation => "Router Solicitation",
                Self::RouterAdvertisement => "Router Advertisement",
                Self::NeighborSolicitation => "Neighbor Solicitation",
                Self::NeighborAdvertisement => "Neighbor Advertisement",
                Self::RedirectMessage => "Redirect Message",
                Self::RouterRenumbering => "Router Renumbering",
                Self::IcmpNodeInformationQuery => "ICMP Node Information Query",
                Self::IcmpNodeInformationResponse => "ICMP Node Information Response",
                Self::InverseNeighborDiscoverySolicitation =>
                    "Inverse Neighbor Discovery Solicitation Message",
                Self::InverseNeighborDiscoveryAdvertisement =>
                    "Inverse Neighbor Discovery Advertisement Message",
                Self::Multicast2ListenerDiscoveryReports =>
                    "Multicast Listener Discovery V2 reports",
                Self::HomeAgentAddressDiscovryRequest =>
                    "Home Agent Address Discovery Request Message",
                Self::HomeAgentAddressDiscovryReply => "Home Agent Address Discovery Reply Message",
                Self::MobilePrefixSolicitation => "Mobile Prefix Solicitation",
                Self::MobilePrefixAdvertisement => "Mobile Prefix Advertisement",
                Self::CertificationPathSolicitation => "Certification Path Solicitation",
                Self::CertificationPathAdvertisement => "Certification Path Advertisement",
                Self::MulticastRouterAdvertisement => "Multicast Router Advertisement",
                Self::MulticastRouterSolicitation => "Multicast Router Solicitation",
                Self::MulticastRouterTermination => "Multicast Router Termination",
                Self::RplControl => "RPL Control Message",
                Self::PrivateExperimentation3 => "Private experimentation",
                Self::PrivateExperimentation4 => "Private experimentation",
                Self::Reserved2 => "Reserved for expansion of ICMPv6 informational messages",
            }
        )
    }
}

impl From<u8> for IcmpType {
    fn from(value: u8) -> Self {
        match value {
            DESTINATION_UNREACHABLE => IcmpType::DestinationUnreachable,
            PACKET_TOO_BIG => IcmpType::PacketTooBig,
            TIME_EXCEEDED => IcmpType::TimeExceeded,
            PARAMETER_PROBLEM => IcmpType::ParameterProblem,
            PRIVATE_EXPERIMENTATION1 => IcmpType::PrivateExperimentation1,
            PRIVATE_EXPERIMENTATION2 => IcmpType::PrivateExperimentation2,
            RESERVED1 => IcmpType::Reserved1,
            ECHO_REQUEST => IcmpType::EchoRequest,
            ECHO_REPLY => IcmpType::EchoReply,
            MULTICAST_LISTENER_QUERY => IcmpType::MulticastListenerQuery,
            MULTICAST_LISTENER_REPORT => IcmpType::MulticastListenerReport,
            MULTICAST_LISTENER_DONE => IcmpType::MulticastListenerDone,
            ROUTER_SOLICITATION => IcmpType::RouterSolicitation,
            ROUTER_ADVERTISEMENT => IcmpType::RouterAdvertisement,
            NEIGHBOR_SOLICITATION => IcmpType::NeighborSolicitation,
            NEIGHBOR_ADVERTISEMENT => IcmpType::NeighborAdvertisement,
            REDIRECT_MESSAGE => IcmpType::RedirectMessage,
            ROUTER_RENUMBERING => IcmpType::RouterRenumbering,
            ICMP_NODE_INFORMATION_QUERY => IcmpType::IcmpNodeInformationQuery,
            ICMP_NODE_INFORMATION_RESPONSE => IcmpType::IcmpNodeInformationResponse,
            INVERSE_NEIGHBOR_DISCOVERY_SOLICITATION => {
                IcmpType::InverseNeighborDiscoverySolicitation
            }
            INVERSE_NEIGHBOR_DISCOVERY_ADVERTISEMENT => {
                IcmpType::InverseNeighborDiscoveryAdvertisement
            }
            MULTICAST2_LISTENER_DISCOVERY_REPORTS => IcmpType::Multicast2ListenerDiscoveryReports,
            HOME_AGENT_ADDRESS_DISCOVRY_REQUEST => IcmpType::HomeAgentAddressDiscovryRequest,
            HOME_AGENT_ADDRESS_DISCOVRY_REPLY => IcmpType::HomeAgentAddressDiscovryReply,
            MOBILE_PREFIX_SOLICITATION => IcmpType::MobilePrefixSolicitation,
            MOBILE_PREFIX_ADVERTISEMENT => IcmpType::MobilePrefixAdvertisement,
            CERTIFICATION_PATH_SOLICITATION => IcmpType::CertificationPathSolicitation,
            CERTIFICATION_PATH_ADVERTISEMENT => IcmpType::CertificationPathAdvertisement,
            MULTICAST_ROUTER_ADVERTISEMENT => IcmpType::MulticastRouterAdvertisement,
            MULTICAST_ROUTER_SOLICITATION => IcmpType::MulticastRouterSolicitation,
            MULTICAST_ROUTER_TERMINATION => IcmpType::MulticastRouterTermination,
            RPL_CONTROL => IcmpType::RplControl,
            PRIVATE_EXPERIMENTATION3 => IcmpType::PrivateExperimentation3,
            PRIVATE_EXPERIMENTATION4 => IcmpType::PrivateExperimentation4,
            RESERVED2 => IcmpType::Reserved2,
            _ => IcmpType::Reserved2,
        }
    }
}

impl From<IcmpType> for u8 {
    fn from(value: IcmpType) -> u8 {
        value as u8
    }
}
