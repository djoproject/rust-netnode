use crate::layers;
use crate::layers::l2::ethernet;
use crate::layers::l3;
use crate::layers::l3::icmpv6::sub_process::neighbor_advertisement;
use crate::layers::l3::icmpv6::Header;
use crate::layers::l3::icmpv6::IcmpType;
use crate::layers::l3::ipv4;
use crate::layers::l3::ipv6;
use crate::utils::buffer;

use log::warn;

const NEIGHBOR_SOLICITATION_CODE: u8 = 0x00_u8;
const NEIGHBOR_ADVERTISEMENT_CODE: u8 = 0x00_u8;

#[repr(C)]
#[derive(Debug, Copy, Clone)]
struct Message {
    _reserved: u32,
    pub target: [u8; 16],
}

pub struct NeighborSolicitation {
    mac: ethernet::Address,
}

impl NeighborSolicitation {
    pub fn new(mac: ethernet::Address) -> Self {
        Self { mac }
    }
}

impl NeighborSolicitation {
    fn compute_checksum(
        &self,
        buffer: buffer::Buffer,
        answer: &mut buffer::Buffer,
        meta: layers::ProcessMeta,
    ) {
        let mut checksum = ipv4::Checksum::new();
        let ipv6_hdr: &ipv6::Header = buffer.to_header(meta.previous.unwrap().start_at);
        let ns_message: &Message = buffer.to_header(meta.start_at + 4);

        // add the pseudoheader to checksum
        checksum.add_data(&ns_message.target);
        checksum.add_data(&ipv6_hdr.src_addr);
        checksum.add_data(&32u32.to_be_bytes()); // IcmpV6 length ( = header + payload + options)
        checksum.add_data(&[0, 0, 0, ipv4::Protocol::ICMPv6.into()]);

        // add icmp header + payload to checksum
        checksum.add_data(&answer[meta.answer_at..]);

        // Computing IcmpV6 checksum
        let icmp_answer_hdr: &mut Header = answer.to_header_mut(meta.answer_at);
        icmp_answer_hdr.checksum = checksum.compute().to_be();
    }

    fn build_target_link_layer_address_option(&self, answer: &mut [u8]) {
        answer[0] = 0x02;
        answer[1] = 0x01;
        answer[2..8].copy_from_slice(&self.mac.octets)
    }
}

impl l3::Layer for NeighborSolicitation {}

impl layers::Layer for NeighborSolicitation {
    fn get_meta_type(&self) -> layers::ProcessMetaType {
        return layers::ProcessMetaType::ICMPv6;
    }

    fn process<'a>(
        &self,
        system: &'a mut layers::ProcessSystem,
        buffer: buffer::Buffer,
        meta: layers::ProcessMeta,
    ) -> (Option<layers::ProcessResult>, Option<buffer::Buffer>) {
        let icmp_hdr: &Header = buffer.to_header(meta.start_at);
        if icmp_hdr.code != NEIGHBOR_SOLICITATION_CODE {
            warn!("NDP received unmanaged code: {:#04x}", icmp_hdr.code);
            return (None, None);
        }

        let ns_message: &Message = buffer.to_header(meta.start_at + 4);

        // XXX should extract ns options here, starting at byte 20

        let answer_option = system.get_next_available_buffer(meta.answer_at + 32);

        if answer_option.is_none() {
            return (None, None);
        }

        let mut answer = answer_option.unwrap();

        // Prepare IcmpV6 header
        let icmp_answer_hdr: &mut Header = answer.to_header_mut(meta.answer_at);
        icmp_answer_hdr.init(IcmpType::NeighborAdvertisement, NEIGHBOR_ADVERTISEMENT_CODE);

        // Prepare Neighbor Advertisement message & options
        let na_message: &mut neighbor_advertisement::Message =
            answer.to_header_mut(meta.answer_at + 4);
        na_message.init(false, true, true);
        na_message.target = ns_message.target;

        self.build_target_link_layer_address_option(&mut answer[meta.answer_at + 24..]);

        // Computing IcmpV6 checksum
        self.compute_checksum(buffer, &mut answer, meta);
        (None, Some(answer))
    }
}
