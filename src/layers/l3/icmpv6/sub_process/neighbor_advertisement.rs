use crate::layers;
use crate::layers::l3;
use crate::utils::buffer;

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct Message {
    pub flags: u8,
    _reserved1: u8,
    _reserved2: u16,
    pub target: [u8; 16],
}

impl Message {
    pub fn init(&mut self, router_flag: bool, sollicited_flag: bool, override_flag: bool) {
        self.flags = 0x00_u8;
        self._reserved1 = 0x00_u8;
        self._reserved2 = 0x0000_u16;

        if router_flag {
            self.flags |= 0x80;
        }

        if sollicited_flag {
            self.flags |= 0x40;
        }

        if override_flag {
            self.flags |= 0x20;
        }
    }
}

pub struct NeighborAdvertisement;

impl NeighborAdvertisement {
    pub fn new() -> Self {
        Self {}
    }
}

impl l3::Layer for NeighborAdvertisement {}

impl layers::Layer for NeighborAdvertisement {
    fn get_meta_type(&self) -> layers::ProcessMetaType {
        return layers::ProcessMetaType::ICMPv6;
    }

    fn process<'a>(
        &self,
        _system: &'a mut layers::ProcessSystem,
        _buffer: buffer::Buffer,
        _meta: layers::ProcessMeta,
    ) -> (Option<layers::ProcessResult>, Option<buffer::Buffer>) {
        // XXX won't do a thing for now, implement it later
        (None, None)
    }
}
