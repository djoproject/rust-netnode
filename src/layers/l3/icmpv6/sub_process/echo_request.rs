use crate::layers;
use crate::layers::l3;
use crate::layers::l3::icmpv6::Header;
use crate::layers::l3::icmpv6::IcmpType;
use crate::layers::l3::ipv4;
use crate::layers::l3::ipv6;
use crate::utils::buffer;

use log::warn;

const ECHO_REQUEST_TYPE: u8 = 0x00_u8;
const ECHO_REPLY_TYPE: u8 = 0x00_u8;

pub struct EchoRequest;

impl EchoRequest {
    pub fn new() -> Self {
        Self {}
    }
}

impl l3::Layer for EchoRequest {}

impl layers::Layer for EchoRequest {
    fn get_meta_type(&self) -> layers::ProcessMetaType {
        return layers::ProcessMetaType::ICMPv6;
    }

    fn process<'a>(
        &self,
        system: &'a mut layers::ProcessSystem,
        buffer: buffer::Buffer,
        meta: layers::ProcessMeta,
    ) -> (Option<layers::ProcessResult>, Option<buffer::Buffer>) {
        let icmp_hdr: &Header = buffer.to_header(meta.start_at);
        if icmp_hdr.code != ECHO_REQUEST_TYPE {
            warn!("received unmanaged code: {:#04x}", icmp_hdr.code);
            return (None, None);
        }

        let answer_option =
            system.get_next_available_buffer(meta.answer_at + buffer.len() - meta.start_at);

        if answer_option.is_none() {
            return (None, None);
        }

        let mut answer = answer_option.unwrap();

        // Copy payload
        for index in 4..(buffer.len() - meta.start_at) {
            answer[meta.answer_at + index] = buffer[meta.start_at + index];
        }

        // Init header
        let icmp_answer_hdr: &mut Header = answer.to_header_mut(meta.answer_at);
        icmp_answer_hdr.init(IcmpType::EchoReply, ECHO_REPLY_TYPE);

        // Init checksum
        let mut checksum = ipv4::Checksum::new();
        let ipv6_hdr: &ipv6::Header = buffer.to_header(meta.previous.unwrap().start_at);

        // Add the pseudoheader to checksum
        checksum.add_data(&ipv6_hdr.dst_addr);
        checksum.add_data(&ipv6_hdr.src_addr);
        checksum.add_data(&((buffer.len() - meta.start_at) as u32).to_be_bytes());
        checksum.add_data(&[0, 0, 0, ipv4::Protocol::ICMPv6.into()]);

        // Add icmpv6 header + data to checksum
        checksum.add_data(&answer[meta.answer_at..]);

        let icmp_answer_hdr: &mut Header = answer.to_header_mut(meta.answer_at);
        icmp_answer_hdr.checksum = checksum.compute().to_be();
        (None, Some(answer))
    }
}
