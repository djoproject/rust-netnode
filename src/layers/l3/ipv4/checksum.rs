pub struct Checksum {
    pub sum: u32,
}

impl Checksum {
    pub fn new() -> Self {
        return Self { sum: 0 };
    }

    pub fn add_data(&mut self, data: &[u8]) {
        let mut index: usize = 0;

        while index < data.len() - 1 {
            self.sum += ((data[index] as u32) << 8) + (data[index + 1] as u32);
            index += 2;
        }

        if data.len() % 2 == 1 {
            self.sum += (data[data.len() - 1] as u32) << 8;
        }
    }

    fn compute_sum(&self) -> u16 {
        let mut sum = self.sum;

        while sum > 0xFFFF {
            let carry = sum >> 16;
            sum = sum & 0xFFFF;
            sum += carry;
        }

        sum as u16
    }

    #[allow(dead_code)]
    pub fn compute(&self) -> u16 {
        0xFFFF ^ self.compute_sum()
    }

    pub fn verify(&self) -> bool {
        self.compute_sum() == 0xFFFF
    }

    #[allow(dead_code)]
    pub fn reset(&mut self) {
        self.sum = 0;
    }
}
