use crate::layers;
use crate::layers::l2;
use crate::layers::l2::ethernet;
use crate::layers::l3;
use crate::layers::l3::ipv4;
use crate::layers::l3::ipv4::Checksum;
use crate::layers::l3::ipv4::Header;
use crate::layers::Protocol;
use crate::utils::buffer;
use crate::utils::result;

use std::collections::HashMap;
use std::collections::HashSet;
use std::net::Ipv4Addr;

use log::warn;

pub struct Process {
    checksum_enabled: bool,
    protocol_subprocess: HashMap<ipv4::Protocol, Box<dyn layers::Layer>>,
    main_address: Ipv4Addr,
    addresses: HashSet<[u8; 4]>,
    dropped_subprocess: Option<Box<dyn layers::Layer>>,
}

impl Process {
    // TODO(address) will also need the l2 layer to add the ipv4 address
    //  IDEA 1: pass l2 ref to constructor
    //  IDEA 2: create a set_main_address method
    //  IDEA 3: create a function to pick the best address to answer
    //  IDEA 4:
    pub fn new(address: Ipv4Addr, checksum_enabled: bool) -> Self {
        let mut process = Self {
            checksum_enabled,
            main_address: address,
            protocol_subprocess: HashMap::new(),
            addresses: HashSet::new(),
            dropped_subprocess: None,
        };

        process.addresses.insert(address.octets());
        process
    }

    pub fn add_protocol_process(
        &mut self,
        protocol: ipv4::Protocol,
        process: Box<dyn layers::Layer>,
    ) {
        self.protocol_subprocess.insert(protocol, process);
    }

    pub fn set_dropped_process(&mut self, process: Box<dyn layers::Layer>) {
        self.dropped_subprocess = Some(process);
    }
}

impl l3::Layer for Process {
    fn add_address(&mut self, address: &[u8]) -> result::Result<()> {
        if address.len() != 4 {
            return Err(result::Error::new(result::ErrorKind::WrongLengthAddress(
                4,
                address.len(),
            )));
        }

        self.addresses.insert(address[0..4].try_into().unwrap());
        Ok(())
    }

    fn update_lower_layer(&mut self, l2: &mut dyn l2::Layer) -> result::Result<()> {
        let l2_meta_type = l2.get_meta_type();

        // Add every unicast ipv4 address to ARP process
        if l2_meta_type == layers::ProcessMetaType::ARP {
            for ipv4_addr in &self.addresses {
                // Skip multicast address
                if ipv4_addr[0] > 223 && ipv4_addr[0] < 240 {
                    continue;
                }

                l2.add_address(ipv4_addr)?;
            }
        } else if l2_meta_type == layers::ProcessMetaType::Ethernet {
            // Add MAC ipv4 all host address
            l2.add_address(ethernet::DefaultAddress::IPv4MulticastAllHosts.octets())?;

            // Add MAC for multicast address
            for ipv4_addr in &self.addresses {
                // Skip unicast address
                if !(ipv4_addr[0] > 223 && ipv4_addr[0] < 240) {
                    continue;
                }

                let mac: [u8; 6] = [
                    0x01,
                    0x00,
                    0x5E,
                    ipv4_addr[1] & 0xEF,
                    ipv4_addr[2],
                    ipv4_addr[3],
                ];
                l2.add_address(&mac)?;
            }
        } else {
            return Err(result::Error::new(
                result::ErrorKind::UnsuportedProcessMetaType(l2_meta_type),
            ));
        }

        Ok(())
    }
}

impl layers::Layer for Process {
    fn get_meta_type(&self) -> layers::ProcessMetaType {
        return layers::ProcessMetaType::IPv4;
    }

    fn process<'a>(
        &self,
        system: &'a mut layers::ProcessSystem,
        buffer: buffer::Buffer,
        meta: layers::ProcessMeta,
    ) -> (Option<layers::ProcessResult>, Option<buffer::Buffer>) {
        let ipv4_min_size = meta.start_at + 20;
        if buffer.len() < ipv4_min_size {
            warn!(
                "Packet too small to contains ipv4, expected {}, got {}",
                ipv4_min_size,
                buffer.len()
            );
            return (None, None);
        }

        // Compute then check checksum
        let ipv4_hdr: &Header = buffer.to_header(meta.start_at);
        let protocol_start = ipv4_hdr.get_header_len();

        if self.checksum_enabled {
            let mut checksum = Checksum::new();
            checksum.add_data(&buffer[meta.start_at..meta.start_at + protocol_start]);

            if !checksum.verify() {
                warn!("invalid checksum, packet dropped.");
                return (None, None);
            }
        }

        let build_next_meta =
            || layers::ProcessMeta::from_meta(&meta, protocol_start, self.get_meta_type());

        // Does this process manage this address ?
        if !self.addresses.contains(&ipv4_hdr.dst_addr) {
            if let Some(process) = &self.dropped_subprocess {
                process.process(system, buffer, build_next_meta());
            }
            return (None, None);
        }

        // Does it manage the carried protocol ?
        let result: Option<buffer::Buffer>;
        let protocol = ipv4_hdr.protocol;
        let dst_addr = ipv4_hdr.src_addr;

        if let Some(process) = self.protocol_subprocess.get(&ipv4_hdr.protocol) {
            let meta = layers::ProcessMeta::from_meta(
                &meta,
                protocol_start,
                ipv4_hdr.protocol.get_meta_type(),
            );
            (_, result) = process.process(system, buffer, meta);
        } else {
            if let Some(process) = &self.dropped_subprocess {
                process.process(system, buffer, build_next_meta());
            }
            return (None, None);
        }

        // Is there an answer to return ?
        if let Some(mut answer) = result {
            let length: u16 = (answer.len() - meta.answer_at) as u16;
            let ipv4_hdr_answer: &mut Header = answer.to_header_mut(meta.answer_at);

            // Initialize header
            // TODO(address): should not have a main_address but a system to compute
            // the best suitable src address from the address pool
            ipv4_hdr_answer.init(length, protocol);
            ipv4_hdr_answer.src_addr = self.main_address.octets();
            ipv4_hdr_answer.dst_addr = dst_addr;

            // Compute checksum
            let mut checksum = Checksum::new();
            checksum.add_data(&answer[meta.answer_at..meta.answer_at + 20]);
            let checksum_computed = checksum.compute();

            let ipv4_hdr_answer: &mut Header = answer.to_header_mut(meta.answer_at);
            ipv4_hdr_answer.checksum = checksum_computed.to_be();
            return (None, Some(answer));
        }
        (None, None)
    }
}
