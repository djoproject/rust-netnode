use crate::layers;
use crate::layers::l3::ipv4;

use std::fmt;
use std::net::Ipv4Addr;

// src: https://datatracker.ietf.org/doc/html/rfc791
//
//    0               1               2               3
//    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//   |Version|  IHL  |   DSCP    |ECN|          Total Length         |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//   |         Identification        |Flags|      Fragment Offset    |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//   |  Time to Live |    Protocol   |         Header Checksum       |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//   |                       Source Address                          |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//   |                    Destination Address                        |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//   |                    Options                    |    Padding    |
//     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct Header {
    pub version_ihl: u8,
    pub dscp_ecn: u8,
    pub tot_len: u16,
    pub id: u16,
    pub frag_off: u16,
    pub ttl: u8,
    pub protocol: ipv4::Protocol,
    pub checksum: u16,
    pub src_addr: [u8; 4], // XXX can't directly use Ipv4Addr struct here, they are not repr(C)
    pub dst_addr: [u8; 4],
}

impl Header {
    pub fn get_header_len(&self) -> usize {
        ((self.version_ihl & 0x0f) as usize) * 4
    }

    pub fn init(&mut self, length: u16, protocol: ipv4::Protocol) {
        self.version_ihl = 0x45;
        self.dscp_ecn = 0x0;
        self.tot_len = length.to_be();
        self.id = 0;
        self.frag_off = 0;
        self.ttl = 0x40;
        self.protocol = protocol;
        self.checksum = 0;
    }

    pub fn get_src(&self) -> Ipv4Addr {
        Ipv4Addr::new(
            self.src_addr[0],
            self.src_addr[1],
            self.src_addr[2],
            self.src_addr[3],
        )
    }
    pub fn get_dst(&self) -> Ipv4Addr {
        Ipv4Addr::new(
            self.dst_addr[4],
            self.dst_addr[5],
            self.dst_addr[6],
            self.dst_addr[7],
        )
    }
}

impl fmt::Display for Header {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "src: {}, dst: {}, protocol: {}",
            self.get_src(),
            self.get_dst(),
            self.protocol
        )
    }
}

impl layers::Header for Header {}
