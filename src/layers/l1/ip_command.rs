use crate::layers::l2::ethernet;

use std::io;
use std::io::ErrorKind;
use std::net::IpAddr;
use std::process::Command;

// e.g.: ip link tap0 up
pub fn set_interface_status(iname: &str, status: bool) -> io::Result<()> {
    let status_str: &str;

    if status {
        status_str = "up";
    } else {
        status_str = "down";
    }

    Command::new("ip")
        .args(["link", "set", iname, status_str])
        .output()?;

    Ok(())
}

// e.g.: ip neigh add "fe80::22" lladdr 06:00:00:00:00:05 dev tap0
pub fn set_neighbor_ip_address(
    iname: &str,
    ip_address: &IpAddr,
    mac_address: &ethernet::Address,
) -> io::Result<()> {
    Command::new("ip")
        .args([
            "neigh",
            "add",
            &format!("{}", ip_address),
            "lladdr",
            &format!("{}", mac_address),
            "dev",
            iname,
        ])
        .output()?;

    Ok(())
}

// e.g.: ip addr add 192.168.222.1/24 dev tap0
pub fn set_ip_address(iname: &str, ip_address: &IpAddr, mask: u8) -> io::Result<()> {
    let mask_upper_limit: u8;
    let str_address: String;

    match ip_address {
        IpAddr::V4(address) => {
            mask_upper_limit = 32;
            str_address = address.to_string();
        }
        IpAddr::V6(address) => {
            mask_upper_limit = 128;
            str_address = address.to_string();
        }
    }

    if mask > mask_upper_limit {
        return Err(io::Error::new(
            ErrorKind::InvalidInput,
            format!(
                "Invalid IP mask, expected a value between 0 and {}, got {}",
                mask_upper_limit, mask
            ),
        ));
    }

    Command::new("ip")
        .args([
            "addr",
            "add",
            &format!("{}/{}", &str_address, mask),
            "dev",
            iname,
        ])
        .output()?;

    Ok(())
}
