use crate::layers;
use crate::layers::l1;
use crate::layers::l1::tuntap;
use crate::utils::buffer;

use log::warn;

/*
 * see TUN_PKT_STRIP in
 * https://elixir.bootlin.com/linux/latest/source/drivers/net/tun.c
 */
const TUN_PKT_STRIP: u16 = 0x0001;

pub struct Process {
    next_process: Option<Box<dyn layers::Layer>>,
}

impl Process {
    pub fn new(next_process: Box<dyn layers::Layer>) -> Self {
        Self {
            next_process: Some(next_process),
        }
    }
}

impl l1::Layer for Process {}

impl layers::Layer for Process {
    fn get_meta_type(&self) -> layers::ProcessMetaType {
        return layers::ProcessMetaType::TunTap;
    }

    fn process<'a>(
        &self,
        system: &'a mut layers::ProcessSystem,
        buffer: buffer::Buffer,
        meta: layers::ProcessMeta,
    ) -> (Option<layers::ProcessResult>, Option<buffer::Buffer>) {
        if buffer.len() < 4 {
            warn!("Less than 4 bytes received, frame skipped.");
            return (None, None);
        }

        let header: &tuntap::Header = buffer.to_header(0);

        /*
         * Check the 2-bytes flags
         * see TUN_PKT_STRIP in
         * https://elixir.bootlin.com/linux/latest/source/drivers/net/tun.c
         */
        if header.flags != 0x0000 {
            if (header.flags & TUN_PKT_STRIP) != 0 {
                // not enough place allocated in user space to store the frame
                warn!("Buffer too small, frame dropped.");
            } else {
                warn!("Unknown flags: {}", header.flags);
            }
            return (None, None);
        }

        // Process
        // FIXME why does it want 4 empty bytes at the front of the frame ??
        //  pretty sure it wasn't the case before.  Does it come from the rust
        //  implementation ?

        if let Some(process) = &self.next_process {
            let next_meta =
                layers::ProcessMeta::from_meta(&meta, 4, layers::ProcessMetaType::Ethernet);
            return process.process(system, buffer, next_meta);
        }

        return (None, None);
    }
}
