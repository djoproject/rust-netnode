use crate::layers;
use crate::layers::l2::ethernet;

use std::fmt;

/*
 * A frame minimal size is supposed to be 4
 *  2-bytes flags
 *  2-bytes ethertype
 */

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct Header {
    pub flags: u16,

    // XXX it seems the O.S. returns etype in BE, even on a LE system
    pub etype: ethernet::EtherType,
}

impl fmt::Display for Header {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Flags: {:#06x}, etype: {}", self.flags, self.etype)
    }
}

impl layers::Header for Header {}
