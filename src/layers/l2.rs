use crate::layers;
use crate::layers::l1;
use crate::layers::l2;
use crate::utils::result;

pub mod arp;
pub mod ethernet;

pub trait Layer: layers::Layer {
    fn add_address(&mut self, _address: &[u8]) -> result::Result<()> {
        return Err(result::Error::new(
            result::ErrorKind::MethodNotSupportedForThisLayer("l2", "add_address"),
        ));
    }

    fn update_lower_layer(&mut self, _l1: &mut dyn l1::Layer) -> result::Result<()> {
        Ok(())
    }

    fn update_layer(&mut self, _l2: &mut dyn l2::Layer) -> result::Result<()> {
        Ok(())
    }
}
