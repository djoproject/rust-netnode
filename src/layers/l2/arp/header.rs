use crate::layers;
use crate::layers::l2::arp::HardwareType;
use crate::layers::l2::arp::Operation;
use crate::layers::l2::ethernet;

use std::fmt;

/*
 * TOTAL LENGTH: 28
 *
    0: Htype u16 // 1=ethernet
    2:  Ptype u16 // same as ethertype, must be 0x0800 for IPv4 request
    4:  Hlen  u8 // must be 6, Hardware len
    5:  Plen  u8 // must be 4, Protocol len
    6:  Oper  u16 // 1=request, 2=reply
    8:  SHA   [u8; 6] // Source Hardware Address
    14: SPA   [u8; 4] // Source Protocol Address
    18: THA   [u8; 6] // Target Hardware Address
    24: TPA   [u8; 4] // Target Protocol Address
*/

// https://www.iana.org/assignments/arp-parameters/arp-parameters.xhtml
// for htype & oper enum

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct Header {
    pub htype: HardwareType,
    pub ptype: ethernet::EtherType,
    pub hlen: u8,
    pub plen: u8,
    pub oper: Operation,
    pub sha: [u8; 6],
    pub spa: [u8; 4],
    pub tha: [u8; 6],
    pub tpa: [u8; 4],
}

impl fmt::Display for Header {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "ARP datagram: oper {}, htype {}, ptye {}",
            self.oper, self.htype, self.ptype
        )
    }
}

impl layers::Header for Header {}
