use std::fmt;

const RESERVED1: u16 = 0x0000_u16.to_be();
const ETHERNET: u16 = 0x0001_u16.to_be();
const EXPERIMENTALETHERNET: u16 = 0x0002_u16.to_be();
const AX_25: u16 = 0x0003_u16.to_be();
const PROTEONPRONETTOKENRING: u16 = 0x0004_u16.to_be();
const CHAOS: u16 = 0x0005_u16.to_be();
const IEEE802: u16 = 0x0006_u16.to_be();
const ARCNET: u16 = 0x0007_u16.to_be();
const HYPERCHANNEL: u16 = 0x0008_u16.to_be();
const LANSTAR: u16 = 0x0009_u16.to_be();
const AUTONETSHORTADDRESS: u16 = 0x000A_u16.to_be();
const LOCALTALK: u16 = 0x000B_u16.to_be();
const LOCALNET: u16 = 0x000C_u16.to_be();
const ULTRALINK: u16 = 0x000D_u16.to_be();
const SMDS: u16 = 0x000E_u16.to_be();
const FRAMERELAY: u16 = 0x000F_u16.to_be();
const ATM1: u16 = 0x0010_u16.to_be();
const HDLC: u16 = 0x0011_u16.to_be();
const FIBRECHANNEL: u16 = 0x0012_u16.to_be();
const ATM2: u16 = 0x0013_u16.to_be();
const SERIALLINE: u16 = 0x0014_u16.to_be();
const ATM3: u16 = 0x0015_u16.to_be();
const MIL_STD_188_220: u16 = 0x0016_u16.to_be();
const METRICOM: u16 = 0x0017_u16.to_be();
const IEEE1394_1995: u16 = 0x0018_u16.to_be();
const MAPOS: u16 = 0x0019_u16.to_be();
const TWINAXIAL: u16 = 0x001A_u16.to_be();
const EUI_64: u16 = 0x001B_u16.to_be();
const HIPARP: u16 = 0x001C_u16.to_be();
const IPANDARPOVERISO7816_3: u16 = 0x001D_u16.to_be();
const ARPSEC: u16 = 0x001E_u16.to_be();
const IPSEC: u16 = 0x001F_u16.to_be();
const INFINIBAND: u16 = 0x0020_u16.to_be();
const TIA_102: u16 = 0x0021_u16.to_be();
const WIEGANDINTERFACE: u16 = 0x0022_u16.to_be();
const PUREIP: u16 = 0x0023_u16.to_be();
const HW_EXP1: u16 = 0x0024_u16.to_be();
const HFI: u16 = 0x0025_u16.to_be();
const UNIFIEDBUS: u16 = 0x0026_u16.to_be();
const HW_EXP2: u16 = 0x0100_u16.to_be();
const AETHERNET: u16 = 0x0101_u16.to_be();
const RESERVED2: u16 = 0xFFFF_u16.to_be();

#[repr(u16)]
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum HardwareType {
    Reserved1 = RESERVED1,
    Ethernet = ETHERNET,
    ExperimentalEthernet = EXPERIMENTALETHERNET,
    Ax25 = AX_25,
    ProteonProNETTokenRing = PROTEONPRONETTOKENRING,
    Chaos = CHAOS,
    IEEE802 = IEEE802,
    ARCNET = ARCNET,
    Hyperchannel = HYPERCHANNEL,
    Lanstar = LANSTAR,
    AutonetShortAddress = AUTONETSHORTADDRESS,
    LocalTalk = LOCALTALK,
    LocalNet = LOCALNET,
    Ultralink = ULTRALINK,
    SMDS = SMDS,
    FrameRelay = FRAMERELAY,
    ATM1 = ATM1,
    HDLC = HDLC,
    FibreChannel = FIBRECHANNEL,
    ATM2 = ATM2,
    SerialLine = SERIALLINE,
    ATM3 = ATM3,
    MilStd188_220 = MIL_STD_188_220,
    Metricom = METRICOM,
    IEEE1394_1995 = IEEE1394_1995,
    MAPOS = MAPOS,
    Twinaxial = TWINAXIAL,
    Eui64 = EUI_64,
    HIPARP = HIPARP,
    IPandARPoverISO7816_3 = IPANDARPOVERISO7816_3,
    ARPSec = ARPSEC,
    IPsec = IPSEC,
    InfiniBand = INFINIBAND,
    Tia102 = TIA_102,
    WiegandInterface = WIEGANDINTERFACE,
    PureIP = PUREIP,
    HwExp1 = HW_EXP1,
    HFI = HFI,
    UnifiedBus = UNIFIEDBUS,
    HwExp2 = HW_EXP2,
    AEthernet = AETHERNET,
    Reserved2 = RESERVED2,
}

impl fmt::Display for HardwareType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::Reserved1 => "Reserved (0x0000)",
                Self::Ethernet => "Ethernet (10Mb) (0x0001)",
                Self::ExperimentalEthernet => "Experimental Ethernet (3Mb) (0x0002)",
                Self::Ax25 => "Amateur Radio AX.25 (0x0003)",
                Self::ProteonProNETTokenRing => "Proteon ProNET Token Ring (0x0004)",
                Self::Chaos => "Chaos (0x0005)",
                Self::IEEE802 => "IEEE 802 Networks (0x0006)",
                Self::ARCNET => "ARCNET (0x0007)",
                Self::Hyperchannel => "Hyperchannel (0x0008)",
                Self::Lanstar => "Lanstar (0x0009)",
                Self::AutonetShortAddress => "Autonet Short Address (0x000A)",
                Self::LocalTalk => "LocalTalk (0x000B)",
                Self::LocalNet => "LocalNet (IBM PCNet or SYTEK LocalNET) (0x000C)",
                Self::Ultralink => "Ultra link (0x000D)",
                Self::SMDS => "SMDS (0x000E)",
                Self::FrameRelay => "Frame Relay (0x000F)",
                Self::ATM1 => "Asynchronous Transmission Mode (ATM) (0x0010)",
                Self::HDLC => "HDLC (0x0011)",
                Self::FibreChannel => "Fibre Channel (0x0012)",
                Self::ATM2 => "Asynchronous Transmission Mode (ATM) (0x0013)",
                Self::SerialLine => "Serial Line (0x0014)",
                Self::ATM3 => "Asynchronous Transmission Mode (ATM) (0x0015)",
                Self::MilStd188_220 => "MIL-STD-188-220 (0x0016)",
                Self::Metricom => "Metricom (0x0017)",
                Self::IEEE1394_1995 => "IEEE 1394.1995 (0x0018)",
                Self::MAPOS => "MAPOS (0x0019)",
                Self::Twinaxial => "Twinaxial (0x001A)",
                Self::Eui64 => "EUI-64 (0x001B)",
                Self::HIPARP => "HIPARP (0x001C)",
                Self::IPandARPoverISO7816_3 => "IP and ARP over ISO 7816-3 (0x001D)",
                Self::ARPSec => "ARPSec (0x001E)",
                Self::IPsec => "IPsec tunnel (0x001F)",
                Self::InfiniBand => "InfiniBand (TM) (0x0020)",
                Self::Tia102 => "TIA-102 Project 25 Common Air Interface (CAI) (0x0021)",
                Self::WiegandInterface => "Wiegand Interface (0x0022)",
                Self::PureIP => "Pure IP (0x0023)",
                Self::HwExp1 => "HW_EXP1 (0x0024)",
                Self::HFI => "HFI (0x0025)",
                Self::UnifiedBus => "Unified Bus (UB) (0x0026)",
                Self::HwExp2 => "HW_EXP2 (0x0100)",
                Self::AEthernet => "AEthernet (0x0101)",
                Self::Reserved2 => "Reserved (0xFFFF)",
            }
        )
    }
}

impl From<u16> for HardwareType {
    fn from(value: u16) -> Self {
        match value {
            RESERVED1 => Self::Reserved1,
            ETHERNET => Self::Ethernet,
            EXPERIMENTALETHERNET => Self::ExperimentalEthernet,
            AX_25 => Self::Ax25,
            PROTEONPRONETTOKENRING => Self::ProteonProNETTokenRing,
            CHAOS => Self::Chaos,
            IEEE802 => Self::IEEE802,
            ARCNET => Self::ARCNET,
            HYPERCHANNEL => Self::Hyperchannel,
            LANSTAR => Self::Lanstar,
            AUTONETSHORTADDRESS => Self::AutonetShortAddress,
            LOCALTALK => Self::LocalTalk,
            LOCALNET => Self::LocalNet,
            ULTRALINK => Self::Ultralink,
            SMDS => Self::SMDS,
            FRAMERELAY => Self::FrameRelay,
            ATM1 => Self::ATM1,
            HDLC => Self::HDLC,
            FIBRECHANNEL => Self::FibreChannel,
            ATM2 => Self::ATM2,
            SERIALLINE => Self::SerialLine,
            ATM3 => Self::ATM3,
            MIL_STD_188_220 => Self::MilStd188_220,
            METRICOM => Self::Metricom,
            IEEE1394_1995 => Self::IEEE1394_1995,
            MAPOS => Self::MAPOS,
            TWINAXIAL => Self::Twinaxial,
            EUI_64 => Self::Eui64,
            HIPARP => Self::HIPARP,
            IPANDARPOVERISO7816_3 => Self::IPandARPoverISO7816_3,
            ARPSEC => Self::ARPSec,
            IPSEC => Self::IPsec,
            INFINIBAND => Self::InfiniBand,
            TIA_102 => Self::Tia102,
            WIEGANDINTERFACE => Self::WiegandInterface,
            PUREIP => Self::PureIP,
            HW_EXP1 => Self::HwExp1,
            HFI => Self::HFI,
            UNIFIEDBUS => Self::UnifiedBus,
            HW_EXP2 => Self::HwExp2,
            AETHERNET => Self::AEthernet,
            RESERVED2 => Self::Reserved2,
            _ => Self::Reserved1,
        }
    }
}

impl From<HardwareType> for u16 {
    fn from(value: HardwareType) -> u16 {
        value as u16
    }
}
