use std::fmt;

const RESERVED1: u16 = 0x0000_u16.to_be();
const REQUEST: u16 = 0x0001_u16.to_be();
const REPLY: u16 = 0x0002_u16.to_be();
const REQUEST_REVERSE: u16 = 0x0003_u16.to_be();
const REPLY_REVERSE: u16 = 0x0004_u16.to_be();
const DRARP_REQUEST: u16 = 0x0005_u16.to_be();
const DRARP_REPLY: u16 = 0x0006_u16.to_be();
const DRARP_ERROR: u16 = 0x0007_u16.to_be();
const INARP_REQUEST: u16 = 0x0008_u16.to_be();
const INARP_REPLY: u16 = 0x0009_u16.to_be();
const ARP_NAK: u16 = 0x000A_u16.to_be();
const MARS_REQUEST: u16 = 0x000B_u16.to_be();
const MARS_MULTI: u16 = 0x000C_u16.to_be();
const MARS_MSERV: u16 = 0x000D_u16.to_be();
const MARS_JOIN: u16 = 0x000E_u16.to_be();
const MARS_LEAVE: u16 = 0x000F_u16.to_be();
const MARS_NAK: u16 = 0x0010_u16.to_be();
const MARS_UNSERV: u16 = 0x0011_u16.to_be();
const MARS_SJOIN: u16 = 0x0012_u16.to_be();
const MARS_SLEAVE: u16 = 0x0013_u16.to_be();
const MARS_GROUPLIST_REQUEST: u16 = 0x0014_u16.to_be();
const MARS_GROUPLIST_REPLY: u16 = 0x0015_u16.to_be();
const MARS_REDIRECT_MAP: u16 = 0x0016_u16.to_be();
const MAPOS_UNARP: u16 = 0x0017_u16.to_be();
const OP_EXP1: u16 = 0x0018_u16.to_be();
const OP_EXP2: u16 = 0x0019_u16.to_be();
const RESERVED2: u16 = 0xffff_u16.to_be();

#[repr(u16)]
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum Operation {
    Reserved1 = RESERVED1,
    REQUEST = REQUEST,
    REPLY = REPLY,
    RequestReverse = REQUEST_REVERSE,
    ReplyReverse = REPLY_REVERSE,
    DRARPRequest = DRARP_REQUEST,
    DRARPReply = DRARP_REPLY,
    DRARPError = DRARP_ERROR,
    InARPRequest = INARP_REQUEST,
    InARPReply = INARP_REPLY,
    ARPNAK = ARP_NAK,
    MARSRequest = MARS_REQUEST,
    MARSMulti = MARS_MULTI,
    MARSMServ = MARS_MSERV,
    MARSJoin = MARS_JOIN,
    MARSLeave = MARS_LEAVE,
    MARSNAK = MARS_NAK,
    MARSUnserv = MARS_UNSERV,
    MARSSJoin = MARS_SJOIN,
    MARSSLeave = MARS_SLEAVE,
    MARSGrouplistRequest = MARS_GROUPLIST_REQUEST,
    MARSGrouplistReply = MARS_GROUPLIST_REPLY,
    MARSRedirectMap = MARS_REDIRECT_MAP,
    MAPOSUNARP = MAPOS_UNARP,
    OPEXP1 = OP_EXP1,
    OPEXP2 = OP_EXP2,
    Reserved2 = RESERVED2,
}

impl fmt::Display for Operation {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::Reserved1 => "Reserved (0x0000)",
                Self::REQUEST => "REQUEST (0x0001)",
                Self::REPLY => "REPLY (0x0002)",
                Self::RequestReverse => "request Reverse (0x0003)",
                Self::ReplyReverse => "reply Reverse (0x0004)",
                Self::DRARPRequest => "DRARP-Request (0x0005)",
                Self::DRARPReply => "DRARP-Reply (0x0006)",
                Self::DRARPError => "DRARP-Error (0x0007)",
                Self::InARPRequest => "InARP-Request (0x0008)",
                Self::InARPReply => "InARP-Reply (0x0009)",
                Self::ARPNAK => "ARP-NAK (0x000A)",
                Self::MARSRequest => "MARS-Request (0x000B)",
                Self::MARSMulti => "MARS-Multi (0x000C)",
                Self::MARSMServ => "MARS-MServ (0x000D)",
                Self::MARSJoin => "MARS-Join (0x000E)",
                Self::MARSLeave => "MARS-Leave (0x000F)",
                Self::MARSNAK => "MARS-NAK (0x0010)",
                Self::MARSUnserv => "MARS-Unserv (0x0011)",
                Self::MARSSJoin => "MARS-SJoin (0x0012)",
                Self::MARSSLeave => "MARS-SLeave (0x0013)",
                Self::MARSGrouplistRequest => "MARS-Grouplist-Request (0x0014)",
                Self::MARSGrouplistReply => "MARS-Grouplist-Reply (0x0015)",
                Self::MARSRedirectMap => "MARS-Redirect-Map (0x0016)",
                Self::MAPOSUNARP => "MAPOS-UNARP (0x0017)",
                Self::OPEXP1 => "OP_EXP1 (0x0018)",
                Self::OPEXP2 => "OP_EXP2 (0x0019)",
                Self::Reserved2 => "Reserved (0xffff)",
            }
        )
    }
}

impl From<u16> for Operation {
    fn from(value: u16) -> Self {
        match value {
            RESERVED1 => Self::Reserved1,
            REQUEST => Self::REQUEST,
            REPLY => Self::REPLY,
            REQUEST_REVERSE => Self::RequestReverse,
            REPLY_REVERSE => Self::ReplyReverse,
            DRARP_REQUEST => Self::DRARPRequest,
            DRARP_REPLY => Self::DRARPReply,
            DRARP_ERROR => Self::DRARPError,
            INARP_REQUEST => Self::InARPRequest,
            INARP_REPLY => Self::InARPReply,
            ARP_NAK => Self::ARPNAK,
            MARS_REQUEST => Self::MARSRequest,
            MARS_MULTI => Self::MARSMulti,
            MARS_MSERV => Self::MARSMServ,
            MARS_JOIN => Self::MARSJoin,
            MARS_LEAVE => Self::MARSLeave,
            MARS_NAK => Self::MARSNAK,
            MARS_UNSERV => Self::MARSUnserv,
            MARS_SJOIN => Self::MARSSJoin,
            MARS_SLEAVE => Self::MARSSLeave,
            MARS_GROUPLIST_REQUEST => Self::MARSGrouplistRequest,
            MARS_GROUPLIST_REPLY => Self::MARSGrouplistReply,
            MARS_REDIRECT_MAP => Self::MARSRedirectMap,
            MAPOS_UNARP => Self::MAPOSUNARP,
            OP_EXP1 => Self::OPEXP1,
            OP_EXP2 => Self::OPEXP2,
            RESERVED2 => Self::Reserved2,
            _ => Self::Reserved1,
        }
    }
}

impl From<Operation> for u16 {
    fn from(value: Operation) -> u16 {
        value as u16
    }
}
