use crate::layers;
use crate::layers::l2;
use crate::layers::l2::arp::HardwareType;
use crate::layers::l2::arp::Header;
use crate::layers::l2::arp::Operation;
use crate::layers::l2::ethernet;
use crate::utils::buffer;
use crate::utils::result;

use log::{debug, warn};

use std::collections::HashSet;

pub struct Process {
    ipv4_addresses: HashSet<[u8; 4]>,
    mac_address: [u8; 6],
}

impl Process {
    pub fn new(mac: ethernet::Address) -> Self {
        return Self {
            ipv4_addresses: HashSet::new(),
            mac_address: mac.octets,
        };
    }
}

impl l2::Layer for Process {
    fn update_layer(&mut self, l2: &mut dyn l2::Layer) -> result::Result<()> {
        // Only the ethernet layer is managed
        let l2_meta_type = l2.get_meta_type();
        if l2_meta_type != layers::ProcessMetaType::Ethernet {
            return Err(result::Error::new(
                result::ErrorKind::UnsuportedProcessMetaType(l2_meta_type),
            ));
        }

        l2.add_address(ethernet::DefaultAddress::Broadcast.octets())?;
        Ok(())
    }

    fn add_address(&mut self, address: &[u8]) -> result::Result<()> {
        if address.len() != 4 {
            return Err(result::Error::new(result::ErrorKind::WrongLengthAddress(
                4,
                address.len(),
            )));
        }

        self.ipv4_addresses
            .insert(address[0..4].try_into().unwrap());
        Ok(())
    }
}

impl layers::Layer for Process {
    fn get_meta_type(&self) -> layers::ProcessMetaType {
        return layers::ProcessMetaType::ARP;
    }

    fn process<'a>(
        &self,
        system: &'a mut layers::ProcessSystem,
        buffer: buffer::Buffer,
        meta: layers::ProcessMeta,
    ) -> (Option<layers::ProcessResult>, Option<buffer::Buffer>) {
        let arp_min_size = meta.start_at + 28;
        if buffer.len() < arp_min_size {
            warn!(
                "Packet too small to contains arp, expected {}, got {}",
                arp_min_size,
                buffer.len()
            );
            return (None, None);
        }

        let header: &Header = buffer.to_header(meta.start_at);

        if header.htype != HardwareType::Ethernet {
            debug!("Receive an ARP frame with htype different from ethernet.");
            return (None, None);
        }

        if header.ptype != ethernet::EtherType::IPv4 {
            debug!("Receive an ARP frame with ptype different from ipv4.");
            return (None, None);
        }

        if header.hlen != 0x6 {
            debug!(
                "Receive an ARP frame with Hlen different from 6, got {}",
                header.hlen
            );
            return (None, None);
        }

        if header.plen != 0x4 {
            debug!(
                "Receive an ARP frame with Plen different from 4, got {}",
                header.plen
            );
            return (None, None);
        }

        if header.oper != Operation::REQUEST {
            debug!(
                "Receive an ARP frame with Oper different from REQUEST, got {}",
                header.oper
            );
            return (None, None);
        }

        if !self.ipv4_addresses.contains(&header.tpa) {
            debug!("Receive an ARP frame with target ip not managed");
            return (None, None);
        }

        let answer_option = system.get_next_available_buffer(meta.answer_at + 28);
        if answer_option.is_none() {
            return (None, None);
        }

        let mut answer = answer_option.unwrap();

        let header_answer: &mut Header = answer.to_header_mut(meta.answer_at);
        header_answer.htype = header.htype;
        header_answer.ptype = header.ptype;
        header_answer.hlen = header.hlen;
        header_answer.plen = header.plen;
        header_answer.oper = Operation::REPLY;
        header_answer.spa = header.tpa;
        header_answer.tpa = header.spa;
        header_answer.tha = header.sha;
        header_answer.sha = self.mac_address;
        (None, Some(answer))
    }
}
