use crate::layers;
use crate::layers::l2;
use crate::layers::l2::ethernet;
use crate::layers::l2::ethernet::Header;
use crate::layers::Protocol;
use crate::utils::buffer;
use crate::utils::result;

use log::warn;

use std::collections::HashMap;
use std::collections::HashSet;

pub struct Process {
    mac: ethernet::Address,
    etype_subprocess: HashMap<ethernet::EtherType, Box<dyn layers::Layer>>,
    llc_subprocess: Option<Box<dyn layers::Layer>>,
    dropped_subprocess: Option<Box<dyn layers::Layer>>,
    oui_addresses: HashSet<[u8; 3]>,
    mac_addresses: HashSet<[u8; 6]>,
}

impl Process {
    // XXX address management: keep the mac as constructor argument ?
    // I would say yes for now.
    pub fn new(mac: ethernet::Address) -> Self {
        let mut ethernet_process = Process {
            mac,
            oui_addresses: HashSet::new(),
            mac_addresses: HashSet::new(),
            etype_subprocess: HashMap::new(),
            llc_subprocess: None,
            dropped_subprocess: None,
        };

        ethernet_process
            .mac_addresses
            .insert(ethernet_process.mac.octets);
        ethernet_process
    }

    pub fn add_etype_process(
        &mut self,
        etype: ethernet::EtherType,
        process: Box<dyn layers::Layer>,
    ) {
        self.etype_subprocess.insert(etype, process);
    }

    pub fn set_llc_default_process(&mut self, process: Box<dyn layers::Layer>) {
        self.llc_subprocess = Some(process);
    }

    pub fn set_dropped_process(&mut self, process: Box<dyn layers::Layer>) {
        self.dropped_subprocess = Some(process);
    }
}

impl l2::Layer for Process {
    fn add_address(&mut self, address: &[u8]) -> result::Result<()> {
        if address.len() == 3 {
            self.oui_addresses.insert(address[0..3].try_into().unwrap());
        } else if address.len() == 6 {
            self.mac_addresses.insert(address[0..6].try_into().unwrap());
        } else {
            return Err(result::Error::new(result::ErrorKind::WrongLengthAddress(
                6,
                address.len(),
            )));
        }

        Ok(())
    }
}

impl layers::Layer for Process {
    fn get_meta_type(&self) -> layers::ProcessMetaType {
        return layers::ProcessMetaType::Ethernet;
    }

    fn process<'a>(
        &self,
        system: &'a mut layers::ProcessSystem,
        buffer: buffer::Buffer,
        meta: layers::ProcessMeta,
    ) -> (Option<layers::ProcessResult>, Option<buffer::Buffer>) {
        // dst is at 0, size 6
        // src is at 6, size 6
        // ethertype is at 12, size 2

        if buffer.len() - meta.start_at < 14 {
            warn!("Frame too small, dropped.");
            return (None, None);
        }

        let header: &Header = buffer.to_header(meta.start_at);
        let build_next_meta = || layers::ProcessMeta::from_meta(&meta, 14, self.get_meta_type());

        // Check dst address
        if !self.mac_addresses.contains(&header.dst.octets) {
            if !self.oui_addresses.contains(&header.dst.octets[..3]) {
                if let Some(process) = &self.dropped_subprocess {
                    process.process(system, buffer, build_next_meta());
                }
                return (None, None);
            }
        }

        // XXX We need the ethertype to be converted into the local system
        // endianess to be able to make size comparison on it (see bellow).
        // The etype is in BE.  Converting it again in BE will keep it unchanged
        // on BE system, and will convert it to LE on LE system.
        let ethertype: u16 = (header.etype as u16).to_be();

        // Manage dropped frames
        if (ethertype < 0x0600 && self.llc_subprocess.is_none())
            || (ethertype >= 0x0600 && self.etype_subprocess.get(&header.etype).is_none())
        {
            if let Some(process) = &self.dropped_subprocess {
                process.process(system, buffer, build_next_meta());
            }
            return (None, None);
        }

        // Manage frame
        let mut result: Option<buffer::Buffer> = None;
        let dst: ethernet::Address = header.src;
        let etype = header.etype;

        if ethertype < 0x0600 {
            if let Some(process) = &self.llc_subprocess {
                (_, result) = process.process(system, buffer, build_next_meta());
            }
        } else if let Some(process) = self.etype_subprocess.get(&header.etype) {
            let meta = layers::ProcessMeta::from_meta(&meta, 14, header.etype.get_meta_type());
            (_, result) = process.process(system, buffer, meta);
        }

        if let Some(mut answer) = result {
            let header_answer: &mut Header = answer.to_header_mut(meta.answer_at);
            header_answer.src = self.mac;
            header_answer.dst = dst;
            header_answer.etype = etype;
            return (None, Some(answer));
        }
        (None, None)
    }
}
