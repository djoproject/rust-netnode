use crate::layers;

use std::fmt;

// https://internals.rust-lang.org/t/pre-rfc-enum-from-integer/6348
// I took the long approach, maybe a better way to create this enum will
// emerge later.

// TODO(complete) update list from https://www.cavebear.com/archive/cavebear/Ethernet/type.html

// Sources:
//  * https://en.wikipedia.org/wiki/EtherType
//  * ...

const IPV4: u16 = 0x0800_u16.to_be();
const ARP: u16 = 0x0806_u16.to_be();
const WAKEONLAN: u16 = 0x0842_u16.to_be();
const STREAMRESERVATIONPROCOTOL: u16 = 0x22EA_u16.to_be();
const AVTP: u16 = 0x22F0_u16.to_be();
const TRILL: u16 = 0x22F3_u16.to_be();
const DECMOPRC: u16 = 0x6002_u16.to_be();
const DECNETPHASE4: u16 = 0x6003_u16.to_be();
const DECLAT: u16 = 0x6004_u16.to_be();
const RARP: u16 = 0x8035_u16.to_be();
const APPLETALK: u16 = 0x809B_u16.to_be();
const AARP: u16 = 0x80F3_u16.to_be();
const VLAN: u16 = 0x8100_u16.to_be();
const SLPP: u16 = 0x8102_u16.to_be();
const VLACP: u16 = 0x8103_u16.to_be();
const IPX1: u16 = 0x8137_u16.to_be();
const IPX2: u16 = 0x8138_u16.to_be();
const QNXQNET: u16 = 0x8204_u16.to_be();
const IPV6: u16 = 0x86DD_u16.to_be();
const ETHERNETFLOWCONTROL: u16 = 0x8808_u16.to_be();
const LACP: u16 = 0x8809_u16.to_be();
const COBRANET: u16 = 0x8819_u16.to_be();
const MPLSUNICAST: u16 = 0x8847_u16.to_be();
const MPLSMULTICAST: u16 = 0x8848_u16.to_be();
const PPPOEDISCOVERY: u16 = 0x8863_u16.to_be();
const PPPOESESSION: u16 = 0x8864_u16.to_be();
const JUMBOFRAMES: u16 = 0x8870_u16.to_be();
const HOMEPLUG1_0MME: u16 = 0x887B_u16.to_be();
const IEEE802_1X: u16 = 0x888E_u16.to_be();
const PROFINET: u16 = 0x8892_u16.to_be();
const HYPERSCSI: u16 = 0x889A_u16.to_be();
const AOE: u16 = 0x88A2_u16.to_be();
const ETHERCAT: u16 = 0x88A4_u16.to_be();
const QINQ: u16 = 0x88A8_u16.to_be();
const ETHERNETPOWERLINK: u16 = 0x88AB_u16.to_be();
const GOOSE: u16 = 0x88B8_u16.to_be();
const GSE: u16 = 0x88B9_u16.to_be();
const SV: u16 = 0x88BA_u16.to_be();
const MIKROTIKROMON: u16 = 0x88BF_u16.to_be();
const LLDP: u16 = 0x88CC_u16.to_be();
const SERCOS3: u16 = 0x88CD_u16.to_be();
const WSMP: u16 = 0x88DC_u16.to_be();
const HOMEPLUGAVMME: u16 = 0x88E1_u16.to_be();
const MRP: u16 = 0x88E3_u16.to_be();
const IEEE802_1AE: u16 = 0x88E5_u16.to_be();
const PTP: u16 = 0x88F7_u16.to_be();
const NCSI: u16 = 0x88F8_u16.to_be();
const PRP: u16 = 0x88FB_u16.to_be();
const IEEE802_1AG: u16 = 0x8902_u16.to_be();
const FCOE: u16 = 0x8906_u16.to_be();
const FCOEINIT: u16 = 0x8914_u16.to_be();
const ROCE: u16 = 0x8915_u16.to_be();
const TTE: u16 = 0x891D_u16.to_be();
const IEEE1905_1: u16 = 0x893a_u16.to_be();
const HSR: u16 = 0x892F_u16.to_be();
const ETHERNETTESTING: u16 = 0x9000_u16.to_be();
const QINQOLD: u16 = 0x9100_u16.to_be();
const VERITASLLT: u16 = 0xCAFE_u16.to_be();
const REDUNDANCYTAG: u16 = 0xF1C1_u16.to_be();

#[repr(u16)]
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum EtherType {
    Unknown = 0x0000_u16,
    IPv4 = IPV4,
    ARP = ARP,
    WakeOnLAN = WAKEONLAN,
    StreamReservationProtocol = STREAMRESERVATIONPROCOTOL,
    AVTP = AVTP,
    TRILL = TRILL,
    DECMOPRC = DECMOPRC,
    DECnetPhase4 = DECNETPHASE4,
    DECLAT = DECLAT,
    RARP = RARP,
    AppleTalk = APPLETALK,
    AARP = AARP,
    Vlan = VLAN,
    SLPP = SLPP,
    VLACP = VLACP,
    IPX1 = IPX1,
    IPX2 = IPX2,
    QNXQnet = QNXQNET,
    IPv6 = IPV6,
    EthernetFlowControl = ETHERNETFLOWCONTROL,
    LACP = LACP,
    CobraNet = COBRANET,
    MPLSUnicast = MPLSUNICAST,
    MPLSMulticast = MPLSMULTICAST,
    PPPoEDiscovery = PPPOEDISCOVERY,
    PPPoESession = PPPOESESSION,
    JumboFrames = JUMBOFRAMES,
    HomePlug1_0MME = HOMEPLUG1_0MME,
    IEEE802_1X = IEEE802_1X,
    PROFINET = PROFINET,
    HyperSCSI = HYPERSCSI,
    AoE = AOE,
    EtherCAT = ETHERCAT,
    QinQ = QINQ,
    EthernetPowerlink = ETHERNETPOWERLINK,
    GOOSE = GOOSE,
    GSE = GSE,
    SV = SV,
    MikrotikRoMON = MIKROTIKROMON,
    LLDP = LLDP,
    SERCOS3 = SERCOS3,
    WSMP = WSMP,
    HomePlugAVMME = HOMEPLUGAVMME,
    MRP = MRP,
    IEEE802_1AE = IEEE802_1AE,
    PTP = PTP,
    NCSI = NCSI,
    PRP = PRP,
    IEEE802_1ag = IEEE802_1AG,
    FCoE = FCOE,
    FCoEInit = FCOEINIT,
    RoCE = ROCE,
    TTE = TTE,
    IEEE1905_1 = IEEE1905_1,
    HSR = HSR,
    EthernetTesting = ETHERNETTESTING,
    QinQOld = QINQOLD,
    VeritasLLT = VERITASLLT,
    RedundancyTag = REDUNDANCYTAG,
}

impl fmt::Display for EtherType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::IPv4 => "IPv4 (0x0800)",
                Self::ARP => "ARP (0x0806)",
                Self::WakeOnLAN => "WakeOnLAN (0x0842)",
                Self::StreamReservationProtocol => "StreamReservationProtocol (0x22EA)",
                Self::AVTP => "AVTP (0x22F0)",
                Self::TRILL => "TRILL (0x22F3)",
                Self::DECMOPRC => "DECMOPRC (0x6002)",
                Self::DECnetPhase4 => "DECnetPhase4 (0x6003)",
                Self::DECLAT => "DECLAT (0x6004)",
                Self::RARP => "RARP (0x8035)",
                Self::AppleTalk => "AppleTalk (0x809B)",
                Self::AARP => "AARP (0x80F3)",
                Self::Vlan => "Vlan (0x8100)",
                Self::SLPP => "SLPP (0x8102)",
                Self::VLACP => "VLACP (0x8103)",
                Self::IPX1 => "IPX1 (0x8137)",
                Self::IPX2 => "IPX2 (0x8138)",
                Self::QNXQnet => "QNXQnet (0x8204)",
                Self::IPv6 => "IPv6 (0x86DD)",
                Self::EthernetFlowControl => "EthernetFlowControl (0x8808)",
                Self::LACP => "LACP (0x8809)",
                Self::CobraNet => "CobraNet (0x8819)",
                Self::MPLSUnicast => "MPLSUnicast (0x8847)",
                Self::MPLSMulticast => "MPLSMulticast (0x8848)",
                Self::PPPoEDiscovery => "PPPoEDiscovery (0x8863)",
                Self::PPPoESession => "PPPoESession (0x8864)",
                Self::JumboFrames => "JumboFrames (0x8870)",
                Self::HomePlug1_0MME => "HomePlug1_0MME (0x887B)",
                Self::IEEE802_1X => "IEEE802_1X (0x888E)",
                Self::PROFINET => "PROFINET (0x8892)",
                Self::HyperSCSI => "HyperSCSI (0x889A)",
                Self::AoE => "AoE (0x88A2)",
                Self::EtherCAT => "EtherCAT (0x88A4)",
                Self::QinQ => "QinQ (0x88A8)",
                Self::EthernetPowerlink => "EthernetPowerlink (0x88AB)",
                Self::GOOSE => "GOOSE (0x88B8)",
                Self::GSE => "GSE (0x88B9)",
                Self::SV => "SV (0x88BA)",
                Self::MikrotikRoMON => "MikrotikRoMON (0x88BF)",
                Self::LLDP => "LLDP (0x88CC)",
                Self::SERCOS3 => "SERCOS3 (0x88CD)",
                Self::WSMP => "WSMP (0x88DC)",
                Self::HomePlugAVMME => "HomePlugAVMME (0x88E1)",
                Self::MRP => "MRP (0x88E3)",
                Self::IEEE802_1AE => "IEEE802_1AE (0x88E5)",
                Self::PTP => "PTP (0x88F7)",
                Self::NCSI => "NCSI (0x88F8)",
                Self::PRP => "PRP (0x88FB)",
                Self::IEEE802_1ag => "IEEE802_1ag (0x8902)",
                Self::FCoE => "FCoE (0x8906)",
                Self::FCoEInit => "FCoEInit (0x8914)",
                Self::RoCE => "RoCE (0x8915)",
                Self::TTE => "TTE (0x891D)",
                Self::IEEE1905_1 => "IEEE1905_1 (0x893a)",
                Self::HSR => "HSR (0x892F)",
                Self::EthernetTesting => "EthernetTesting (0x9000)",
                Self::QinQOld => "QinQOld (0x9100)",
                Self::VeritasLLT => "VeritasLLT (0xCAFE)",
                Self::RedundancyTag => "RedundancyTag (0xF1C1)",
                Self::Unknown => "Unknown EtherType",
            }
        )
    }
}
impl layers::Protocol for EtherType {
    fn get_meta_type(&self) -> layers::ProcessMetaType {
        match self {
            Self::IPv4 => layers::ProcessMetaType::IPv4,
            Self::IPv6 => layers::ProcessMetaType::IPv6,
            _ => layers::ProcessMetaType::UnManaged,
        }
    }
}

impl From<u16> for EtherType {
    fn from(value: u16) -> Self {
        match value {
            IPV4 => EtherType::IPv4,
            ARP => EtherType::ARP,
            WAKEONLAN => EtherType::WakeOnLAN,
            STREAMRESERVATIONPROCOTOL => EtherType::StreamReservationProtocol,
            AVTP => EtherType::AVTP,
            TRILL => EtherType::TRILL,
            DECMOPRC => EtherType::DECMOPRC,
            DECNETPHASE4 => EtherType::DECnetPhase4,
            DECLAT => EtherType::DECLAT,
            RARP => EtherType::RARP,
            APPLETALK => EtherType::AppleTalk,
            AARP => EtherType::AARP,
            VLAN => EtherType::Vlan,
            SLPP => EtherType::SLPP,
            VLACP => EtherType::VLACP,
            IPX1 => EtherType::IPX1,
            IPX2 => EtherType::IPX2,
            QNXQNET => EtherType::QNXQnet,
            IPV6 => EtherType::IPv6,
            ETHERNETFLOWCONTROL => EtherType::EthernetFlowControl,
            LACP => EtherType::LACP,
            COBRANET => EtherType::CobraNet,
            MPLSUNICAST => EtherType::MPLSUnicast,
            MPLSMULTICAST => EtherType::MPLSMulticast,
            PPPOEDISCOVERY => EtherType::PPPoEDiscovery,
            PPPOESESSION => EtherType::PPPoESession,
            JUMBOFRAMES => EtherType::JumboFrames,
            HOMEPLUG1_0MME => EtherType::HomePlug1_0MME,
            IEEE802_1X => EtherType::IEEE802_1X,
            PROFINET => EtherType::PROFINET,
            HYPERSCSI => EtherType::HyperSCSI,
            AOE => EtherType::AoE,
            ETHERCAT => EtherType::EtherCAT,
            QINQ => EtherType::QinQ,
            ETHERNETPOWERLINK => EtherType::EthernetPowerlink,
            GOOSE => EtherType::GOOSE,
            GSE => EtherType::GSE,
            SV => EtherType::SV,
            MIKROTIKROMON => EtherType::MikrotikRoMON,
            LLDP => EtherType::LLDP,
            SERCOS3 => EtherType::SERCOS3,
            WSMP => EtherType::WSMP,
            HOMEPLUGAVMME => EtherType::HomePlugAVMME,
            MRP => EtherType::MRP,
            IEEE802_1AE => EtherType::IEEE802_1AE,
            PTP => EtherType::PTP,
            NCSI => EtherType::NCSI,
            PRP => EtherType::PRP,
            IEEE802_1AG => EtherType::IEEE802_1ag,
            FCOE => EtherType::FCoE,
            FCOEINIT => EtherType::FCoEInit,
            ROCE => EtherType::RoCE,
            TTE => EtherType::TTE,
            IEEE1905_1 => EtherType::IEEE1905_1,
            HSR => EtherType::HSR,
            ETHERNETTESTING => EtherType::EthernetTesting,
            QINQOLD => EtherType::QinQOld,
            VERITASLLT => EtherType::VeritasLLT,
            REDUNDANCYTAG => EtherType::RedundancyTag,
            _ => EtherType::Unknown,
        }
    }
}

impl From<EtherType> for u16 {
    fn from(value: EtherType) -> u16 {
        value as u16
    }
}
