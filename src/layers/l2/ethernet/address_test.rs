use super::Address;
use std::str::FromStr;

#[test]
fn test_parse_address_case1() {
    let result = Address::from_str("112233445566");
    assert!(result.is_ok());
    assert_eq!(result.unwrap().octets, [0x11, 0x22, 0x33, 0x44, 0x55, 0x66]);
}

#[test]
fn test_parse_address_case2() {
    let result = Address::from_str("11:22:33:44:55:66");
    assert!(result.is_ok());
    assert_eq!(result.unwrap().octets, [0x11, 0x22, 0x33, 0x44, 0x55, 0x66]);
}

#[test]
fn test_parse_address_case3() {
    let result = Address::from_str("1122.3344.5566");
    assert!(result.is_ok());
    assert_eq!(result.unwrap().octets, [0x11, 0x22, 0x33, 0x44, 0x55, 0x66]);
}

#[test]
fn test_parse_address_invalid_length() {
    let result = Address::from_str("11223344");
    assert!(result.is_err());
    // TODO(test) check message
}

#[test]
fn test_parse_address_invalid_byte() {
    let result = Address::from_str("11ZZ33445566");
    assert!(result.is_err());
    // TODO(test) check message
}
