use crate::utils::result;

use std::{fmt, str::FromStr};

#[repr(C)]
#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub struct Address {
    pub octets: [u8; 6],
}

impl Address {
    pub fn new(a: u8, b: u8, c: u8, d: u8, e: u8, f: u8) -> Self {
        Address {
            octets: [a, b, c, d, e, f],
        }
    }

    fn new_empty() -> Self {
        Address {
            octets: [0, 0, 0, 0, 0, 0],
        }
    }

    pub fn octets(&self) -> &[u8] {
        &self.octets
    }
}

impl fmt::Display for Address {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{:02x}:{:02x}:{:02x}:{:02x}:{:02x}:{:02x}",
            &self.octets[0],
            &self.octets[1],
            &self.octets[2],
            &self.octets[3],
            &self.octets[4],
            &self.octets[5],
        )
    }
}

impl FromStr for Address {
    type Err = result::Error;

    fn from_str(s: &str) -> Result<Address, result::Error> {
        // case1: 060000000005
        if s.len() == 12 {
            let mut a = Address::new_empty();

            for i in 0..6 {
                match u8::from_str_radix(&s[i * 2..i * 2 + 2], 16) {
                    Err(_e) => {
                        return Err(result::Error::new(result::ErrorKind::InvalidStringAddress(
                            format!("Invalid byte at index {}", i),
                        )));
                    }
                    Ok(v) => a.octets[i] = v,
                }
            }

            return Ok(a);
        }
        // case2a: 06:00:00:00:00:05
        // case2a: 06-00-00-00-00-05
        else if s.len() == 17 {
            let mut offset: usize = 0;
            let mut a = Address::new_empty();

            for i in 0..6 {
                match u8::from_str_radix(&s[(i * 2) + offset..(i * 2 + 2) + offset], 16) {
                    Err(_e) => {
                        return Err(result::Error::new(result::ErrorKind::InvalidStringAddress(
                            format!("Invalid byte at index {}", i),
                        )));
                    }
                    Ok(v) => a.octets[i] = v,
                }
                offset += 1;
            }

            return Ok(a);
        }
        // case3: 0600.0000.0005
        else if s.len() == 14 {
            let mut offset: usize = 0;
            let mut a = Address::new_empty();

            for i in 0..6 {
                match u8::from_str_radix(&s[(i * 2) + offset..(i * 2 + 2) + offset], 16) {
                    Err(_e) => {
                        return Err(result::Error::new(result::ErrorKind::InvalidStringAddress(
                            format!("Invalid byte at index {}", i),
                        )));
                    }
                    Ok(v) => a.octets[i] = v,
                }

                if i == 1 || i == 3 {
                    offset += 1;
                }
            }

            return Ok(a);
        }

        Err(result::Error::new(result::ErrorKind::InvalidStringAddress(
            format!(
                "Invalid string size, expected a length of 12, 14, or 15.  Got {}",
                s.len()
            ),
        )))
    }
}

const BROADCAST: [u8; 6] = [0xff, 0xff, 0xff, 0xff, 0xff, 0xff];
const IPV4_MULTICAST_ALL_HOSTS: [u8; 6] = [0x01, 0x00, 0x5e, 0x00, 0x00, 0x01];
const IPV4_MULTICAST_ALL_ROUTERS: [u8; 6] = [0x01, 0x00, 0x5e, 0x00, 0x00, 0x02];
const IPV4_MULTICAST_IGMP: [u8; 6] = [0x01, 0x00, 0x5e, 0x00, 0x00, 0x16];
const IPV4_MULTICAST_MDNS: [u8; 6] = [0x01, 0x00, 0x5e, 0x00, 0x00, 0xfb];
const IPV6_MULTICAST_ALL_HOSTS: [u8; 6] = [0x33, 0x33, 0x00, 0x00, 0x00, 0x01];
const IPV6_MULTICAST_ALL_ROUTERS: [u8; 6] = [0x33, 0x33, 0x00, 0x00, 0x00, 0x02];
const IPV6_MULTICAST_MDNS: [u8; 6] = [0x33, 0x33, 0x00, 0x00, 0x00, 0xfb];

#[allow(dead_code)]
pub enum DefaultAddress {
    Broadcast,
    IPv4MulticastAllHosts,
    IPv4MulticastAllRouters,
    IPv4MulticastIgmp,
    IPv4MulticastMdns,
    IPv6MulticastAllHosts,
    IPv6MulticastAllRouters,
    IPv6MulticastMdns,
    // TODO(complete) Add more ipv4 default addresses
}

impl DefaultAddress {
    pub fn octets(&self) -> &[u8] {
        match self {
            DefaultAddress::Broadcast => &BROADCAST,
            DefaultAddress::IPv4MulticastAllHosts => &IPV4_MULTICAST_ALL_HOSTS,
            DefaultAddress::IPv4MulticastAllRouters => &IPV4_MULTICAST_ALL_ROUTERS,
            DefaultAddress::IPv4MulticastIgmp => &IPV4_MULTICAST_IGMP,
            DefaultAddress::IPv4MulticastMdns => &IPV4_MULTICAST_MDNS,
            DefaultAddress::IPv6MulticastAllHosts => &IPV6_MULTICAST_ALL_HOSTS,
            DefaultAddress::IPv6MulticastAllRouters => &IPV6_MULTICAST_ALL_ROUTERS,
            DefaultAddress::IPv6MulticastMdns => &IPV6_MULTICAST_MDNS,
        }
    }
}

impl Into<Address> for DefaultAddress {
    fn into(self) -> Address {
        let octets = self.octets();
        Address::new(
            octets[0], octets[1], octets[2], octets[3], octets[4], octets[5],
        )
    }
}
