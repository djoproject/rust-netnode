use std::fmt;

#[derive(Copy, Clone, PartialEq, Eq, Hash)]
pub struct OUI {
    pub octets: [u8; 3],
}

impl OUI {
    pub fn new(a: u8, b: u8, c: u8) -> Self {
        OUI { octets: [a, b, c] }
    }
}

impl fmt::Display for OUI {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{:02x}:{:02x}:{:02x}",
            &self.octets[0], &self.octets[1], &self.octets[2],
        )
    }
}

#[allow(dead_code)]
pub enum DefaultOUI {
    IPv4Multicast,
    IPv6Multicast,
    // TODO(complete) add more OUI
}

impl Into<OUI> for DefaultOUI {
    fn into(self) -> OUI {
        match self {
            DefaultOUI::IPv4Multicast => OUI::new(0x01, 0x00, 0x5e),
            DefaultOUI::IPv6Multicast => OUI::new(0x33, 0x33, 0x00),
        }
    }
}
