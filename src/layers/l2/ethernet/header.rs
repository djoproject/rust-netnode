use crate::layers;
use crate::layers::l2::ethernet;

use std::fmt;

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct Header {
    pub dst: ethernet::Address,
    pub src: ethernet::Address,
    pub etype: ethernet::EtherType,
}

impl fmt::Display for Header {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let etype = (self.etype as u16).to_be();

        if etype < 0x600 {
            return write!(
                f,
                "802.3 frame, length: {} bytes, from {}, to {}",
                etype, self.src, self.dst
            );
        }
        let etype_enum = ethernet::EtherType::from(self.etype);

        if etype_enum == ethernet::EtherType::Unknown {
            return write!(
                f,
                "v2 frame, ethertype: {}, from {}, to {}",
                self.etype, self.src, self.dst
            );
        }

        write!(
            f,
            "v2 frame, ethertype: {}, from {}, to {}",
            etype_enum, self.src, self.dst
        )
    }
}

impl layers::Header for Header {}
