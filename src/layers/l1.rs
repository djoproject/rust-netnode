use crate::layers;

pub mod ip_command;
pub mod tuntap;

pub trait Layer: layers::Layer {}
